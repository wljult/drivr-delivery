<?php
use App\Models\Config;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$secret = Config::where('name', 'api_secret')->first()->value;

Route::get('/', function () {
    // return view('welcome');
    return redirect('/login');
});

Auth::routes();

Route::post('/tookan/'.$secret.'/webhook/', 'TookanController@webhook');

Route::group(['middleware' => ['is_approved', 'auth']], function () {
	Route::get('/home', 'CommonController@deliveryList')->name('home');
	Route::get('/profile', 'CommonController@profile')->name('profile');
	Route::put('/profile', 'CommonController@profileUpdate')->name('profile.update');

	Route::group(['prefix' => 'address'], function () {
		Route::get('/', 'CommonController@addressList')->name('address.index');
		Route::post('/datatable','CommonController@addressDatatable')->name('address.datatable');
		Route::get('/create', 'CommonController@addressCreateForm')->name('address.create');
		Route::get('/get', 'CommonController@addressGet')->name('address.get');
		Route::post('/store', 'CommonController@addressStore')->name('address.store');
		Route::get('/{address}/edit', 'CommonController@addressEditForm')->name('address.edit');
		Route::put('/{address}/update', 'CommonController@addressUpdate')->name('address.update');
		Route::delete('/destroy/{address}', 'CommonController@addressDestroy')->name('address.destroy');
	});
	
	Route::group(['prefix' => 'delivery'], function () {
		Route::get('/', 'CommonController@deliveryList')->name('delivery.index');
		Route::post('/datatable','CommonController@deliveryDatatable')->name('delivery.datatable');
		Route::get('/create', 'CommonController@deliveryCreateForm')->name('delivery.create');
		Route::post('/store', 'CommonController@deliveryStore')->name('delivery.store');
		Route::get('/{delivery}/edit', 'CommonController@deliveryEditForm')->name('delivery.edit');
		Route::post('/{delivery}/update', 'CommonController@deliveryUpdate')->name('delivery.update');
		Route::get('/cancel/{delivery}', 'CommonController@deliveryCancel')->name('delivery.cancel');
		Route::get('/repost/{delivery}', 'CommonController@deliveryRepostTookan')->name('delivery.repost');
		Route::get('/detail/{delivery}', 'CommonController@deliveryGetDetailTookan')->name('delivery.detail');
		
	});
});

Route::group(['middleware' => ['is_approved', 'is_admin','auth'], 'prefix' => 'user'], function () {
	Route::get('/', 'CommonController@userList')->name('user.index');
	Route::post('/datatable','CommonController@userDatatable')->name('user.datatable');
	Route::get('/create', 'CommonController@userCreateForm')->name('user.create');
	Route::post('/store', 'CommonController@userStore')->name('user.store');
	Route::get('/{user}/edit', 'CommonController@userEditForm')->name('user.edit');
	Route::put('/{user}/update', 'CommonController@userUpdate')->name('user.update');
	Route::delete('/destroy/{user}', 'CommonController@userDestroy')->name('user.destroy');
	Route::get('/{user}/enable', 'CommonController@userEnable')->name('user.enable');
	Route::get('/{user}/approve', 'CommonController@userApprove')->name('user.approve');
	Route::get('/{user}/deny', 'CommonController@userDeny')->name('user.deny');
});

Route::group(['middleware' => ['is_approved', 'is_admin','auth'], 'prefix' => 'config'], function () {
	Route::get('/', 'CommonController@configList')->name('config.index');
	Route::post('/datatable','CommonController@configDatatable')->name('config.datatable');
	Route::get('/{config}/edit', 'CommonController@configEditForm')->name('config.edit');
	Route::put('/{config}/update', 'CommonController@configUpdate')->name('config.update');
});

Route::group(['middleware' => ['is_approved', 'is_admin','auth'], 'prefix' => 'option'], function () {
	Route::get('/', 'CommonController@optionList')->name('option.index');
	Route::post('/datatable','CommonController@optionDatatable')->name('option.datatable');
	Route::get('/create', 'CommonController@optionCreateForm')->name('option.create');
	Route::post('/store', 'CommonController@optionStore')->name('option.store');
	Route::get('/{option}/edit', 'CommonController@optionEditForm')->name('option.edit');
	Route::put('/{option}/update', 'CommonController@optionUpdate')->name('option.update');
});