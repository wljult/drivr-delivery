<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Config;
use Log;

class updateToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'onemap:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'retrieve onemap\'s token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = Config::where('name', 'email')->first()->value;
        $password = Config::where('name', 'password')->first()->value;

        $req_body = array(
                    "email" => $email,
                    "password" => $password,
                );

        $req_api = curl_init('https://developers.onemap.sg/privateapi/auth/post/getToken');
        curl_setopt_array($req_api, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POSTFIELDS => $req_body
        ));

        $req_response = curl_exec($req_api);
        $req_data = json_decode($req_response, true);
        $token = Config::where('name','token')->first();
        $token->value = $req_data['access_token'];
        $token->save();

        Log::info($req_data['access_token']);
    }
}
