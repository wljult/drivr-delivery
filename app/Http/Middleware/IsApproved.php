<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->is_approved == false) {
            Auth::logout();
            return redirect('login')->with('error', 'Your account is not approved yet by admin. <br>
                Please try again later.');
        }
        
        return $next($request);
    }
}
