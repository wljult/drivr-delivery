<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enums\DeliveryStatus;
use App\Models\Delivery;
use App\Models\Config;
use Log;

class TookanController extends Controller
{
    public function createTask($count, $data, $pickup_longitude, $pickup_latitude)
    {
    	$api_key = Config::where('name', 'api_key')->first()->value;
    	$pickup_datetime = date('m/d/Y h:i A', strtotime($data[0][0]->delivery_date.' '.$data[0][0]->delivery_time.' - 45 minutes'));
    	$delivery_datetime = date('m/d/Y h:i A', strtotime($data[0][0]->delivery_date.' '.$data[0][0]->delivery_time));
    	$pickup_addresses = explode('^', $data[0][0]->pickup_address);
    	$converted_pickup_data = [];
    	$create_task_result = [];

    	foreach ($pickup_addresses as $pickup_data) {
    		$explode = explode(': ', $pickup_data);
    		$converted_pickup_data[$explode[0]] = $explode[1];
    	}

    	foreach ($data as $delivery_data) {
    		$postData = [
    			'api_key' => $api_key,
		    	"order_id" => $delivery_data[0]->order_no,
                "team_id" => "364151",
				"auto_assignment" => "1",
				"job_pickup_phone" => $converted_pickup_data['Contact'],
				"job_pickup_name" => $converted_pickup_data['Name'],
				"job_pickup_address" => $converted_pickup_data['Address'],
				"job_pickup_latitude" => $pickup_latitude,
				"job_pickup_longitude" => $pickup_longitude,
				"job_pickup_datetime" => $pickup_datetime,
				"customer_username" => $delivery_data[0]->name,
				"customer_phone" => $delivery_data[0]->contact,
				"customer_address" => $delivery_data[0]->delivery_address.' #'.$delivery_data[0]->unit_no.' S'.$delivery_data[0]->postal_code,
				"latitude" => $delivery_data[0]->latitude,
				"longitude" => $delivery_data[0]->longitude,
				"job_delivery_datetime" => $delivery_datetime,
				"has_pickup" => "1",
				"has_delivery" => "1",
				"layout_type" => "0",
				"tracking_link" => 1,
				"timezone" => "-480",
				"notify" => 1,
				"geofence" => 0,
				"ride_type" => 0,
                "pickup_custom_field_template" => "Pickup",
		    ];

            if($delivery_data[0]->fee == 10) {
                $postData['custom_field_template'] = "Delivery01";
            }
            elseif ($delivery_data[0]->fee == 14) {
                $postData['custom_field_template'] = "Delivery02";
            }
            elseif($delivery_data[0]->fee == 17) {
                $postData['custom_field_template'] = "Delivery03";   
            }
            elseif ($delivery_data[0]->fee == 22) {
                $postData['custom_field_template'] = "Delivery04";
            }
            elseif ($delivery_data[0]->fee == 24) {
                $postData['custom_field_template'] = "Delivery05";
            }
            else {
                $postData['custom_field_template'] = "Delivery06";
            }   
	        
        	$create_api = curl_init('https://api.tookanapp.com/v2/create_task');
        	//$create_api = curl_init('https://private-anon-fdbb9dd4aa-tookanapi.apiary-proxy.com/v2/create_task');
	        curl_setopt_array($create_api, array(
	            CURLOPT_POST => TRUE,
	            CURLOPT_RETURNTRANSFER => TRUE,
	            CURLOPT_HTTPHEADER => array(
	            'Content-Type: application/json'
	            ),
	            CURLOPT_POSTFIELDS => json_encode($postData)
	        ));
        	$create_response = curl_exec($create_api);
        	$create_result = json_decode($create_response);
        	if ($create_result->status != 200) {
        		$response = [
        			'status' => $create_result->status,
        			'step' => 1,
        			'delivery_key' => $delivery_data['delivery_key'],
        			'message' => $create_result->message, 
        			'data' => [],
        		];
        		array_push($create_task_result, $response);
        	}
        	elseif ($create_response == false) {
        		$response = [
        			'status' => 0,
        			'step' => 1,
        			'delivery_key' => $delivery_data['delivery_key'],
        			'message' => $create_result, 
        			'data' => [],
        		];
        		array_push($create_task_result, $response);
        	}
        	else {
        		$details = $this->getTaskDetails($create_result->data->pickup_job_id, $create_result->data->delivery_job_id);
        		if ($details['status'] != 200) {
        			$response = [
	        			'status' => $details['status'],
	        			'step' => 2,
	        			'delivery_key' => $delivery_data['delivery_key'],
	        			'message' => $details['message'], 
	        			'data' => [],
	        		];
        		}
        		else {
        			$response = $details;
        			$response['delivery_key'] = $delivery_data['delivery_key'];
        		}
        		array_push($create_task_result, $response);
        	}
    	}
    	return $create_task_result;
    }

    public function getTaskDetails($pickup_id, $delivery_id)
    {
    	$api_key = Config::where('name', 'api_key')->first()->value;
    	$postData = [
    		'api_key' => $api_key,
		    "job_ids" => [$delivery_id],
    	];

    	$details_api = curl_init('https://api.tookanapp.com/v2/get_job_details');
    	//$details_api = curl_init('https://private-anon-fdbb9dd4aa-tookanapi.apiary-proxy.com/v2/get_job_details');
        curl_setopt_array($details_api, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
    	$details_response = curl_exec($details_api);
    	$details_result = json_decode($details_response);

    	if ($details_result->status != 200) {
    		$response = [
    			'status' => $details_result->status,
    			'message' => $details_result->message,
    			'data' => [],
    		];
    	}
    	else {
    		$data = $details_result->data[0];
    		$response = [
    			'status' => $details_result->status,
    			'step' => 2,
    			'message' => $details_result->message,
    			'data' => [
    				'delivery_id' => $data->job_id,
    				'pickup_id' => $pickup_id,
    				'job_status' => $data->job_status,
    				'tracking_link' => $data->tracking_link,
    			],
    		];
    	}
    	return $response;
    }

    public function repost($data, $pickup) {
    	$api_key = Config::where('name', 'api_key')->first()->value;
    	$pickup_datetime = date('m/d/Y h:i A', strtotime($data->delivery_date.' '.$data->delivery_time.' - 45 minutes'));
    	$delivery_datetime = date('m/d/Y h:i A', strtotime($data->delivery_date.' '.$data->delivery_time));
    	$create_task_result = [];

		$postData = [
			'api_key' => $api_key,
	    	"order_id" => $data->order_no,
            "team_id" => "364151",
			"auto_assignment" => "1",
			"job_pickup_phone" => $pickup->phone,
			"job_pickup_name" => $pickup->name,
			"job_pickup_address" => $pickup->address,
			"job_pickup_latitude" => $pickup->latitude,
			"job_pickup_longitude" => $pickup->longitude,
			"job_pickup_datetime" => $pickup_datetime,
			"customer_username" => $data->name,
			"customer_phone" => $data->contact,
			"customer_address" => $data->delivery_address.' #'.$data->unit_no.' S'.$data->postal_code,
			"latitude" => $data->latitude,
			"longitude" => $data->longitude,
			"job_delivery_datetime" => $delivery_datetime,
            "pickup_custom_field_template" => "Pickup",
			"has_pickup" => "1",
			"has_delivery" => "1",
			"layout_type" => "0",
			"tracking_link" => 1,
			"timezone" => "-480",
			"notify" => 1,
			"geofence" => 0,
			"ride_type" => 0,
	    ];	        

        if($delivery_data[0]->fee == 10) {
            $postData['custom_field_template'] = "Delivery01";
        }
        elseif ($delivery_data[0]->fee == 14) {
            $postData['custom_field_template'] = "Delivery02";
        }
        elseif($delivery_data[0]->fee == 17) {
            $postData['custom_field_template'] = "Delivery03";   
        }
        elseif ($delivery_data[0]->fee == 22) {
            $postData['custom_field_template'] = "Delivery04";
        }
        elseif ($delivery_data[0]->fee == 24) {
            $postData['custom_field_template'] = "Delivery05";
        }
        else {
            $postData['custom_field_template'] = "Delivery06";
        }      

    	$create_api = curl_init('https://api.tookanapp.com/v2/create_task');
    	//$create_api = curl_init('https://private-anon-fdbb9dd4aa-tookanapi.apiary-proxy.com/v2/create_task');
        curl_setopt_array($create_api, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
    	$create_response = curl_exec($create_api);
    	$create_result = json_decode($create_response);
    	if ($create_result->status != 200) {
    		$response = [
    			'status' => $create_result->status,
    			'step' => 1,
    			'message' => $create_result->message, 
    			'data' => [],
    		];
    		array_push($create_task_result, $response);
    	}
    	elseif ($create_response == false) {
    		$response = [
    			'status' => 0,
    			'step' => 1,
    			'message' => $create_result, 
    			'data' => [],
    		];
    		array_push($create_task_result, $response);
    	}
    	else {
    		$details = $this->getTaskDetails($create_result->data->pickup_job_id, $create_result->data->delivery_job_id);
    		if ($details['status'] != 200) {
    			$response = [
        			'status' => $details['status'],
        			'step' => 2,
        			'message' => $details['message'], 
        			'data' => [],
        		];
    		}
    		else {
    			$response = $details;
    		}
    		array_push($create_task_result, $response);
    	}
    	
    	return $create_task_result;
    }

    public function updateStatus($job_id, $status)
    {
    	$api_key = Config::where('name', 'api_key')->first()->value;
    	$postData = [
    		'api_key' => $api_key,
		    "job_id" => $job_id,
		    "job_status" => $status,
    	];
    	$status_api = curl_init('https://api.tookanapp.com/v2/update_task_status');
        curl_setopt_array($status_api, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));
    	$status_response = curl_exec($status_api);
    	$status_result = json_decode($status_response);
    	$response = [
    		'status' => $status_result->status,
    		'message' => $status_result->message,
    		'data' => [],
    	];

    	return $response;
    }

    public function webhook(Request $request) {
        Log::info($request);
    	$secret = Config::where('name', 'api_secret')->first()->value;
    	if($secret == $request['tookan_shared_secret']) {
            $status = $request['job_status'];
            if($status == DeliveryStatus::SUCCESSFUL || $status == DeliveryStatus::INPROGRESS || $status == DeliveryStatus::STARTED || $status == DeliveryStatus::FAILED || $status == DeliveryStatus::CANCEL || $status == DeliveryStatus::UNASSIGNED ||  $status == DeliveryStatus::ASSIGNED) {
                $delivery = Delivery::where('delivery_id', $request['job_id'])->get();
                if (count($delivery) >= 1) {
                    $update = $delivery->first();
                    $update->status = $status;
                    $update->tracking_link = $request['full_tracking_link'];
                    $update->detail_fail = 0;
                    $update->save();    
                }
            }
    	}
    }
}
