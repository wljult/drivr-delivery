<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\TookanController as Tookan;
use App\Models\Address;
use App\Models\Config;
use App\Models\Delivery;
use App\Models\Fee;
use App\Models\User;
use App\Models\Option;
use App\Models\Enums\DeliveryStatus;
use App\Models\Enums\AdditionalOption;
use App\Mail\NotificationMail;
use App\Mail\ApprovalMail;
use App\Mail\ErrorMail;
use DataTables;
use Session;
use Mail;
use Log;

class CommonController extends Controller
{

    public function uniqueToken()
    {
        $random_string = mt_rand().'+'.uniqid().'+'.mt_rand(1000,9999);
        $hashed_string = hash('sha256', $random_string);

        $user = User::find(auth()->user()->id);
        $user->unique_token = $hashed_string;
        $user->save();

        return $hashed_string;
    }

    public function uniqueTokenValidator($unique_token)
    {
        $user = User::find(auth()->user()->id);
        if ($user->unique_token == $unique_token) {
            $unique_token = $this->uniqueToken();
            $user->unique_token = $unique_token;
            $user->save();
            return true;
        }
        else {
            return false;
        }
    }

	public function userList()
    {
    	return view('vendor.adminlte.admin.user.index');
    }

    public function userDatatable()
    {
    	$users = User::all();
        return Datatables::of($users)
	        ->addIndexColumn()
	        ->addColumn('approved', function($row) {
	        	if (is_null($row->is_approved)) {
                    return 'Pending';
                }
                elseif ($row->is_approved == 0) {
                    return 'Denied';
                }
                else {
                    return 'Approved';
                }
	        })
	        ->addColumn('role', function($row) {
                return $row->is_admin ? "Admin" : "Normal";
	        })
	        ->addColumn('status', function($row) {
                return $row->is_enabled ? "Active" : "Disabled";
	        })
	        ->addColumn('action', function($row) {
	            $btn = "<a href='".route('user.edit', $row->id)."' class='btn btn-primary'>Edit</a>";

                if (is_null($row->is_approved)){
                    $btn .= "<a href='".route('user.approve',$row->id)."' class='btn btn-success'>Approve</a><a href='".route('user.deny',$row->id)."' class='btn btn-danger' onclick='disableButton()' >Deny</a>";
                } 
                elseif ($row->is_approved == 0){
                    $btn .= "<a href='".route('user.approve',$row->id)."' class='btn btn-success' onclick='disableButton()'>Reapproval</a>";
                }

                $btn .= "<a href='".route('user.enable', $row->id)."' onclick='disableButton()' class='btn btn-".($row->is_enabled ? 'danger' : 'success')."'>"
                    .($row->is_enabled ? 'Disable' : 'Enable')."</a>";

	            if (auth()->user()->id != $row->id) {
	            	$btn .= "<button class='btn btn-danger' data-toggle='modal' data-target='#modalDelete'"
                        ." data-id='".$row->id."' data-subject='user' data-function='delete_user'>Delete</button>";
	            }
	            return $btn;
	        })
	        ->rawColumns(['action'])
	        ->make(true);
    }

    public function userCreateForm()
    {
        $unique_token = $this->uniqueToken();
        return view('vendor.adminlte.admin.user.create', compact('unique_token'));
    }

    public function userStore(Request $request)
    {
        $request->validate([
            'name'          => ['required', 'string', 'max:255'],
            'username'      => ['required', 'regex:/^[\w-]*$/', 'min:8', 'max:16', 'unique:users'],
            'email'         => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'      => ['required', 'string', 'min:8', 'confirmed'],
            'is_admin'      => 'required|boolean',
            'is_enabled'    => 'required|boolean',
        ]);

        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('user.index')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->is_admin = $request->is_admin;
        $user->is_enabled = $request->is_enabled;
        $user->is_approved = 1;
        $user->save();

        return redirect()->route('user.index')->with('success', 'User added.');
    }

    public function userEditForm($id)
    {
        $user = User::find($id);
        $unique_token = $this->uniqueToken();
        return view('vendor.adminlte.admin.user.edit', compact('user', 'unique_token'));
    }

    public function userUpdate(Request $request, $id)
    {   
        if ($request->change_password == 1) {
            $request->validate([
                'name'          => ['required', 'string', 'max:255'],
                'username'      => 'required|regex:/^[\w-]*$/|min:8|max:16|unique:users,username,'.$id,
                'email'         => 'required|string|email|max:255|unique:users,email,'.$id,
                'password'      => ['required', 'string', 'min:8', 'confirmed'],
                'is_admin'      => 'required|boolean',
                'is_enabled'    => 'required|boolean',
            ]);
        }
        else {
            $request->validate([
                'name'          => ['required', 'string', 'max:255'],
                'username'      => 'required|regex:/^[\w-]*$/|min:8|max:16|unique:users,username,'.$id,
                'email'         => 'required|string|email|max:255|unique:users,email,'.$id,
                'is_admin'      => 'required|boolean',
                'is_enabled'    => 'required|boolean',  
            ]);
        }

        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('user.index')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        if ($request->change_password == 1) {
           $user->password = bcrypt($request->password);
        }
        $user->is_admin = $request->is_admin;
        $user->is_enabled = $request->is_enabled;
        $user->save();

        return redirect()->route('user.index')->with('success', 'User updated.');
    }

    public function userDestroy($id)
    {
        if (auth()->user()->id != $id) {
            $user = User::find($id);
            $user->delete();
            Session::flash('success', 'User removed.');
        }
        else {
            Session::flash('error', 'Cannot remove yourself.');
        }
        return response()->json(['success'=>'success']);
    }

    public function userApprove($id)
    {
        $user = User::find($id);
        $user->is_approved = 1;
        $user->save();

        $status = 'approved';
        Mail::to($user->email)->send(new ApprovalMail($user, $status));
        return redirect()->back()->with('success', 'User approved.');
    }

    public function userDeny($id)
    {
        $user = User::find($id);
        $user->is_approved = 0;
        $user->save();

        $email = Config::where('name', 'admin_email')->first()->value;
        $status = 'denied';
        Mail::to($user->email)->send(new ApprovalMail($user, $status, $email));
        return redirect()->back()->with('success', 'User denied.');
    }

    public function userEnable($id)
    {   
        $user = User::find($id);
        $user->is_enabled = !$user->is_enabled;
        $user->save();
        return redirect()->back()->with('success', 'User status updated.');
    }

    public function profile()
    {
        $user = User::find(auth()->user()->id);
        $unique_token = $this->uniqueToken();
        return view('vendor.adminlte.user.profile', compact('user', 'unique_token'));
    }

    public function profileUpdate(Request $request)
    {   
        $id = auth()->user()->id;
        if ($request->change_password == 1) {
            $request->validate([
                'name'       => ['required', 'string', 'max:255'],
                'username'   => 'required|regex:/^[\w-]*$/|min:8|max:16|unique:users,username,'.$id,
                'email'      => 'required|string|email|max:255|unique:users,email,'.$id,
                'password'   => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        }
        else {
            $request->validate([
                'name'       => ['required', 'string', 'max:255'],
                'username'   => 'required|regex:/^[\w-]*$/|min:8|max:16|unique:users,username,'.$id,
                'email'      => 'required|string|email|max:255|unique:users,email,'.$id,
            ]);
        }

        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('profile')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $user = User::find($id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        if ($request->change_password == 1) {
           $user->password = bcrypt($request->password);
        }
        $user->save();

        return redirect()->route('profile')->with('success', 'Profile updated.');
    }

    public function addressList()
    {
        return view('vendor.adminlte.address.index');
    }

    public function addressDatatable()
    {
        if (auth()->user()->is_admin) {
            $addresses = Address::all();
        }
        else {
            $addresses = Address::where('user_id', auth()->user()->id)->get();
        }
        
        return Datatables::of($addresses)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                if (!auth()->user()->is_admin) {
                     $btn = "<a href='".route('address.edit', $row->id)."' class='btn btn-primary'>Edit</a>";

                    if (auth()->user()->id == $row->user_id) {
                        $btn .= "<button class='btn btn-danger' data-toggle='modal' data-target='#modalDelete'"
                            ." data-id='".$row->id."' data-subject='address' data-function='delete_address'>Delete</button>";
                    }
                    return $btn;
                }
                else {
                    return "No action available.";
                }
               
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function addressCreateForm()
    {
        $unique_token = $this->uniqueToken();
        if (!auth()->user()->is_admin) {
            return view('vendor.adminlte.address.create', compact('unique_token'));
        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed to create address.');
        }
    }

    public function googleGetLatLong($address)
    {
            $converted_address = str_replace(' ', '%20', $address);
            $google_api_key = Config::where('name', 'google_api_key')->first()->value;
            $url = "https://maps.google.com/maps/api/geocode/json?address=".$converted_address."&components=country:SG&key=".$google_api_key;
            $curl_init = curl_init($url);
            curl_setopt($curl_init, CURLOPT_RETURNTRANSFER, true);
            $curl_exec = curl_exec($curl_init);
            $curl_getinfo = curl_getinfo($curl_init, CURLINFO_HTTP_CODE);
            curl_close($curl_init);

            if($curl_getinfo != 200) {
                $api_response = [['results'=>[]]];
            }
            else {
                $api_response = json_decode($curl_exec);
            }
            
            return $api_response;
    }


    public function addressGet(Request $request)
    {
        if (!auth()->user()->is_admin) {
            $url = "https://developers.onemap.sg/commonapi/search?searchVal=".$request->postal_code."&returnGeom=Y&getAddrDetails=Y&pageNum=1";
            $curl_init = curl_init($url);
            curl_setopt($curl_init, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($curl_init, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl_init, CURLOPT_SSL_VERIFYPEER, 0);
	        $curl_exec = curl_exec($curl_init);
            $curl_getinfo = curl_getinfo($curl_init, CURLINFO_HTTP_CODE);
            curl_close($curl_init);

            $address = json_decode($curl_exec)->results;
            if ($address == null) {
                return response()->json($address);
            }
            else {
                $google_latlong_reponse = $this->googleGetLatLong($address[0]->BLK_NO.' '.$address[0]->ROAD_NAME);

                if(!empty($google_latlong_reponse->results)) {
                    $address[0]->LATITUDE = $google_latlong_reponse->results[0]->geometry->location->lat;
                    $address[0]->LONGITUDE = $google_latlong_reponse->results[0]->geometry->location->lng;
                }
                if ($request->pickup) {
                    $fee = $this->calculateFee($request->pickup, $address[0]);
                    $address[0]->fee = $fee;
                    return response()->json($address[0]);  
                }
                else {
                    return response()->json($address[0]);    
                }
            }
        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed.');
        }                                       
    }

    public function calculateFee($pickup, $delivery)
    {
        if (!auth()->user()->is_admin) {
            $map_api = Config::where('name', 'current_api')->first()->value;

            if($map_api == 'OneMap SG') {
                //onemap Token
                $token = Config::where('name', 'token')->first()->value;
            }
            
            else {
                //google map token
                $token = Config::where('name', 'google_api_key')->first()->value;
            }

            $pickup_address = Address::find($pickup);
            $pickup_latitude = $pickup_address->latitude;
            $pickup_longitude = $pickup_address->longitude;
            $delivery_latitude = $delivery->LATITUDE;
            $delivery_longitude = $delivery->LONGITUDE;

            if($map_api == 'OneMap SG') {
                // onemap get distance
                $get_distance_url = "https://developers.onemap.sg/privateapi/routingsvc/route?start=".$pickup_latitude.",".$pickup_longitude."&end=".$delivery_latitude.",".$delivery_longitude."&routeType=drive&token=".$token."&numItineraries=1";

                $curl_init_distance = curl_init($get_distance_url);
                curl_setopt($curl_init_distance, CURLOPT_RETURNTRANSFER, true);
    	        curl_setopt($curl_init_distance, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl_init_distance, CURLOPT_SSL_VERIFYPEER, 0);
    	        $curl_exec_distance = curl_exec($curl_init_distance);
                $curl_getinfo_distance = curl_getinfo($curl_init_distance, CURLINFO_HTTP_CODE);
                curl_close($curl_init_distance);
                $total_distance = (json_decode($curl_exec_distance))->route_summary->total_distance;
            }

            else {
                // google map get distance
                $get_distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=".$pickup_latitude.",".$pickup_longitude."&destinations=".$delivery_latitude.",".$delivery_longitude."&key=".$token;
                $curl_init_distance = curl_init($get_distance_url);
                curl_setopt($curl_init_distance, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl_init_distance, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl_init_distance, CURLOPT_SSL_VERIFYPEER, 0);
                $curl_exec_distance = curl_exec($curl_init_distance);
                $curl_getinfo_distance = curl_getinfo($curl_init_distance, CURLINFO_HTTP_CODE);
                curl_close($curl_init_distance);
                $total_distance = (json_decode($curl_exec_distance))->rows[0]->elements[0]->distance->value;
            }

            $fee_list = Fee::all();
            foreach ($fee_list as $distance) {
                if ($total_distance >= $distance->min_distance && $total_distance <= $distance->max_distance && $total_distance < 25000) {
                    $fee = $distance->value_in_sgd;
                }
                elseif ($total_distance > 24999) {
                    $get_difference = (ceil(($total_distance - 24999)/1000))*0.3;
                    $fee = 24 + $get_difference;
                }
            }
            return $fee;
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        } 
    }

    public function addressStore(Request $request)
    {
        if (!auth()->user()->is_admin) {
            $request->validate([
                'name'           => ['required', 'string', 'max:255'],
                'postal_code'    => ['required', 'regex:/[0-9]/'],
                'address'        => ['required', 'string', 'max:255'],
                'unit_no'        => ['required', 'string', 'max:255'],
                'phone'          => ['required', 'regex:/[0-9]/'],
                'longitude'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'latitude'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            ]);

            $validate = $this->uniqueTokenValidator($request->unique_token);
                if ($validate == false) {
                    return redirect()->route('address.create')->with('error', 'Please do not click on the submit button more than once.'); 
                }

            $address = new Address;
            $address->user_id = auth()->user()->id;
            $address->name = $request->name;
            $address->postal_code = $request->postal_code;
            $address->address = $request->address;
            $address->unit_no = $request->unit_no;
            $address->phone = $request->phone;
            $address->longitude = $request->longitude;
            $address->latitude = $request->latitude;
            $address->save();

            return redirect()->route('address.index')->with('success', 'Address added.');
        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed.');
        } 
    }

    public function addressEditForm($id)
    {
        $unique_token = $this->uniqueToken();
        $address = Address::find($id);

        if (!auth()->user()->is_admin) {
            return view('vendor.adminlte.address.edit', compact('address', 'unique_token'));
        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed to edit address.');
        }
    }

    public function addressUpdate(Request $request, $id)
    {   
        if (!auth()->user()->is_admin) {
            $request->validate([
                'name'           => ['required', 'string', 'max:255'],
                'postal_code'    => ['required', 'regex:/[0-9]/'],
                'address'        => ['required', 'string', 'max:255'],
                'unit_no'        => ['required', 'string', 'max:255'],
                'phone'          => ['required', 'regex:/[0-9]/'],
                'longitude'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'latitude'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            ]);

            $validate = $this->uniqueTokenValidator($request->unique_token);
            if ($validate == false) {
                return redirect()->route('address.edit')->with('error', 'Please do not click on the submit button more than once.'); 
            }

            $address = Address::find($id);
            if ($address->user_id != auth()->user()->id) {
                return redirect('/address')->with('error', 'You are not authorized to do that action.');
            }
            
            $address->name = $request->name;
            $address->postal_code = $request->postal_code;
            $address->address = $request->address;
            $address->unit_no = $request->unit_no;
            $address->phone = $request->phone;
            $address->longitude = $request->longitude;
            $address->latitude = $request->latitude;
            $address->save();

            return redirect()->route('address.index')->with('success', 'Address updated.');
        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed.');
        }
    }

    public function addressDestroy($id)
    {
        if (!auth()->user()->is_admin) {
            $address = Address::find($id);
            if ($address->user_id != auth()->user()->id) {
                return redirect('/address')->with('error', 'You are not authorized to do that action.');
            }
            
            $address->delete();
            Session::flash('success', 'Address removed.');
            
            return response()->json(['success'=>'success']);

        }
        else {
            return redirect('/address')->with('error', 'Admin is not allowed.');
        }
    }

    public function deliveryList(Request $request)
    {
        $deliveryStatus = DeliveryStatus::getArray();
    	return view('vendor.adminlte.delivery.index', compact('deliveryStatus'));
    }

    public function deliveryDatatable(Request $request)
    {   
        if (auth()->user()->is_admin) {
            if ($request['status_filter'] != 0) {
                if ($request['from_date'] !== NULL && $request['to_date'] !== NULL) {
                $raw_start_date = date('Y-m-d', ($request['from_date']/1000));
                $start_date = date('Y-m-d', strtotime($raw_start_date.' - 1 day'));
                $raw_end_date = date('Y-m-d', ($request['to_date']/1000));
                $end_date = date('Y-m-d', strtotime($raw_end_date.' + 1 day'));
                $deliveries = Delivery::where('delivery_date', '>', $start_date)->where('delivery_date', '<', $end_date)->where('status', $request['status_filter'])->orderBy('created_at', 'desc')->get();
                }
                else {
                    $deliveries = Delivery::where('status', $request['status_filter'])->orderBy('created_at', 'desc')->get();
                }
            }
            else {
                if ($request['from_date'] !== NULL && $request['to_date'] !== NULL) {
                    $raw_start_date = date('Y-m-d', ($request['from_date']/1000));
                    $start_date = date('Y-m-d', strtotime($raw_start_date.' - 1 day'));
                    $raw_end_date = date('Y-m-d', ($request['to_date']/1000));
                    $end_date = date('Y-m-d', strtotime($raw_end_date.' + 1 day'));
                    $deliveries = Delivery::where('delivery_date', '>', $start_date)->where('delivery_date', '<', $end_date)->orderBy('created_at', 'desc')->get();
                }
                else {
                    $deliveries = Delivery::orderBy('created_at', 'desc')->get();
                }
            }        
        }
        else {
            if ($request['status_filter'] != 0) {
                if ($request['from_date'] !== NULL && $request['to_date'] !== NULL) {
                    $raw_start_date = date('Y-m-d', ($request['from_date']/1000));
                    $start_date = date('Y-m-d', strtotime($raw_start_date.' - 1 day'));
                    $raw_end_date = date('Y-m-d', ($request['to_date']/1000));
                    $end_date = date('Y-m-d', strtotime($raw_end_date.' + 1 day'));
                    $deliveries = Delivery::where('poster_id', auth()->user()->id)->where('delivery_date', '>', $start_date)->where('delivery_date', '<', $end_date)->where('status', $request['status_filter'])->orderBy('created_at', 'desc')->get();
                }
                else {
                    $deliveries = Delivery::where('poster_id', auth()->user()->id)->where('status', $request['status_filter'])->orderBy('created_at', 'desc')->get();
                }
            }
            else {
                if ($request['from_date'] !== NULL && $request['to_date'] !== NULL) {
                    $raw_start_date = date('Y-m-d', ($request['from_date']/1000));
                    $start_date = date('Y-m-d', strtotime($raw_start_date.' - 1 day'));
                    $raw_end_date = date('Y-m-d', ($request['to_date']/1000));
                    $end_date = date('Y-m-d', strtotime($raw_end_date.' + 1 day'));
                    $deliveries = Delivery::where('poster_id', auth()->user()->id)->where('delivery_date', '>', $start_date)->where('delivery_date', '<', $end_date)->orderBy('created_at', 'desc')->get();
                }
                else {
                    $deliveries = Delivery::where('poster_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();
                }
            }
        }

        foreach($deliveries as $delivery) {
            $delivery->poster_name = User::find($delivery->poster_id)->name;
        }
        
        return Datatables::of($deliveries)
            ->addIndexColumn()
            ->addColumn('pickup_location', function($row) {
                $locations = explode('^', $row->pickup_address);
                $array = [];
                foreach($locations as $location) {
                    $loc = ['location' => $location];
                    array_push($array, $loc);
                }
                return $array;
            })
            ->addColumn('delivery_location', function($row) {
                $delivery_data = [
                    "Order No: ".$row->order_no,
                    "Name: ".$row->name,
                    "Contact: ".$row->contact,
                    "Address: ".$row->delivery_address.' #'.$row->unit_no.' S'.$row->postal_code,
                ];
                $array2 = [];
                foreach($delivery_data as $data) {
                    $delivery_array = ['data' => $data];
                    array_push($array2, $delivery_array);
                }
                return $array2;
            })
            ->addColumn('delivery_date', function($row) {
                $time = date('d/m/Y', strtotime($row->delivery_date));
                return $time;
            })
            ->addColumn('delivery_time', function($row) {
                $time = date('h:i A', strtotime($row->delivery_time));
                return $time;
            })
            ->addColumn('status', function($row) {
                if ($row->status == DeliveryStatus::CANCEL) {
                    $array = [
                        'class' => 'status-red',
                        'url' => '',
                        'status' => 'Cancelled',
                    ]; 
                }
                elseif ($row->status == DeliveryStatus::PENDING) {
                    $array = [
                        'class' => 'status-gray',
                        'url' => '',
                        'status' => DeliveryStatus::getString($row->status),
                    ];
                }
                elseif ($row->status == DeliveryStatus::SUCCESSFUL) {
                    $array = [
                        'class' => 'status-green',
                        'url' => $row->tracking_link,
                        'status' => DeliveryStatus::getString($row->status),
                    ];
                }
                elseif ($row->status == DeliveryStatus::STARTED || $row->status == DeliveryStatus::INPROGRESS) {
                    $array = [
                        'class' => 'status-blue',
                        'url' => $row->tracking_link,
                        'status' => DeliveryStatus::getString($row->status),
                    ];
                }
                elseif ($row->status == DeliveryStatus::ASSIGNED || $row->status ==DeliveryStatus::DECLINE) {
                    $array = [
                        'class' => 'status-orange',
                        'url' => $row->tracking_link,
                        'status' => DeliveryStatus::getString($row->status),
                    ];
                }
                elseif ($row->status == DeliveryStatus::UNASSIGNED) {
                    $array = [
                        'class' => 'status-orange',
                        'url' => $row->tracking_link,
                        'status' => 'Pending',
                    ];
                }
                elseif ($row->status == DeliveryStatus::PENDING) {
                    $array = [
                        'class' => 'status-black',
                        'url' => $row->tracking_link,
                        'status' => 'N/A',
                    ];
                }
                else {
                    $array = [
                        'class' => 'status-black',
                        'url' => $row->tracking_link,
                        'status' => DeliveryStatus::getString($row->status),
                    ];
                }
                return $array;
            })
            ->addColumn('fee', function($row) {
                if ($row->option_one != 0 && $row->option_two != 0 && $row->option_three != 0 && $row->option_four != 0) {
                    $get_value_one = explode('_', $row->option_one);
                    Log::info($row->option_one );
                    $option_one = Option::find($get_value_one[0])->name;
                    $value_one = $get_value_one[1];
                    $get_value_two = explode('_', $row->option_two);
                    $option_two = Option::find($get_value_two[0])->name;
                    $value_two = $get_value_two[1]; 
                    $get_value_three = explode('_', $row->option_three);
                    $option_three = Option::find($get_value_three[0])->name;
                    $value_three = $get_value_three[1]; 
                    $get_value_four = explode('_', $row->option_four);
                    $option_four = Option::find($get_value_four[0])->name;
                    $value_four = $get_value_four[1];  
                    return $row->total_fee.' ('.$option_one.' $'.$value_one.', '.$option_two.' $'.$value_two.', '.$option_three.' $'.$value_three.', '.$option_four.' $'.$value_four.')';
                }
                elseif ($row->option_one != 0 && $row->option_two != 0 && $row->option_three != 0 && $row->option_four == 0) {
                    $get_value_one = explode('_', $row->option_one);
                    $option_one = Option::find($get_value_one[0])->name;
                    $value_one = $get_value_one[1];
                    $get_value_two = explode('_', $row->option_two);
                    $option_two = Option::find($get_value_two[0])->name;
                    $value_two = $get_value_two[1]; 
                    $get_value_three = explode('_', $row->option_three);
                    $option_three = Option::find($get_value_three[0])->name;
                    $value_three = $get_value_three[1]; 
                    return $row->total_fee.' ('.$option_one.' $'.$value_one.', '.$option_two.' $'.$value_two.', '.$option_three.' $'.$value_three.')';
                }
                elseif ($row->option_one != 0 && $row->option_two != 0 && $row->option_three == 0 && $row->option_four == 0) {
                    $get_value_one = explode('_', $row->option_one);
                    $option_one = Option::find($get_value_one[0])->name;
                    $value_one = $get_value_one[1];
                    $get_value_two = explode('_', $row->option_two);
                    $option_two = Option::find($get_value_two[0])->name;
                    $value_two = $get_value_two[1]; 
                    return $row->total_fee.' ('.$option_one.' $'.$value_one.', '.$option_two.' $'.$value_two.')';
                }
                elseif ($row->option_one != 0 && $row->option_two == 0 && $row->option_three == 0 && $row->option_four == 0) {
                    $get_value_one = explode('_', $row->option_one);
                    $option_one = Option::find($get_value_one[0])->name;
                    $value_one = $get_value_one[1];
                    return $row->total_fee.' ('.$option_one.' $'.$value_one.')';
                }
                else {
                    return $row->total_fee;
                }
            })
            ->addColumn('action', function($row) {

                if (!auth()->user()->is_admin) {
                    $btn = '';
                    if ($row->status != DeliveryStatus::SUCCESSFUL && $row->status != DeliveryStatus::CANCEL && !$row->detail_fail) {
                        $btn = "<button class='btn btn-danger' data-toggle='modal' data-target='#modalCancel'"
                    ." data-id='".$row->id."' data-subject='cancel' data-function='cancel_delivery'>Cancel</button>";
                    }

                    if($row->post_fail && $row->status != DeliveryStatus::CANCEL && $row->status != DeliveryStatus::SUCCESSFUL) {
                         $btn .= "<a href='".route('delivery.repost', $row->id)."' onclick='disableButton()' class='btn btn-primary'>Repost to Tookan</a>";
                    }
                    elseif ($row->detail_fail && $row->status != DeliveryStatus::CANCEL && $row->status != DeliveryStatus::SUCCESSFUL) {
                        $btn .= "<a href='".route('delivery.detail', $row->id)."' onclick='disableButton()' class='btn btn-primary'>Refresh</a>";
                    }
                    if ($btn == '') {
                        return 'No action available';
                    }
                    else {
                        return $btn;
                    }
                }
                else {
                    $btn = "<a href='".route('delivery.edit', $row->id)."' class='btn btn-primary'>Edit</a>";

                    return $btn;
                }
                
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function deliveryCreateForm()
    {
        $unique_token = $this->uniqueToken();
        if (!auth()->user()->is_admin) {
            $addresses = Address::where('user_id', auth()->user()->id)->get();
            if (count($addresses) == 0 || auth()->user()->is_admin) {
                return redirect('/delivery')->with('error', 'You must have at least one address saved to use this function.');
            }
            return view('vendor.adminlte.delivery.create', compact('addresses', 'unique_token'));
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        }
    }

    public function deliveryStore(Request $request)
    {
        if (!auth()->user()->is_admin) {
            $ids_array = [];
            $addresses = Address::where('user_id', auth()->user()->id)->get();
            foreach ($addresses as $address) {
                array_push($ids_array, $address->id);
            }

            $ids = implode(',', $ids_array);

            if ($request->has_address_two && $request->has_address_three) {
                $request->validate([
                    'pickup_address'     => ['required', 'in:'.$ids],

                    'order_no_one'       => ['required', 'string', 'max:255'],
                    'name_one'           => ['required', 'string', 'max:255'],
                    'postal_code_one'    => ['required', 'regex:/[0-9]/'],
                    'address_one'        => ['required', 'string', 'max:255'],
                    'unit_no_one'        => ['required', 'string', 'max:255'],
                    'phone_one'          => ['required', 'regex:/[0-9]/'],
                    'longitude_one'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_one'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'order_no_two'       => ['required', 'string', 'max:255'],
                    'name_two'           => ['required', 'string', 'max:255'],
                    'postal_code_two'    => ['required', 'regex:/[0-9]/'],
                    'address_two'        => ['required', 'string', 'max:255'],
                    'unit_no_two'        => ['required', 'string', 'max:255'],
                    'phone_two'          => ['required', 'regex:/[0-9]/'],
                    'longitude_two'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_two'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'order_no_three'     => ['required', 'string', 'max:255'],
                    'name_three'         => ['required', 'string', 'max:255'],
                    'postal_code_three'  => ['required', 'regex:/[0-9]/'],
                    'address_three'      => ['required', 'string', 'max:255'],
                    'unit_no_three'      => ['required', 'string', 'max:255'],
                    'phone_three'        => ['required', 'regex:/[0-9]/'],
                    'longitude_three'    => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_three'     => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'delivery_date'      => ['required', 'date'],
                    'delivery_time'      => ['required', 'date_format:H:i'],
                    'fee'                => ['required', 'numeric'],
                ]);

                $postal_codes = [
                    'postal_code_one'    => $request->postal_code_one,
                    'postal_code_two'    => $request->postal_code_two,
                    'postal_code_three'  => $request->postal_code_three,  
                ];
            }

            else if ($request->has_address_two && !$request->has_address_three) {
                $request->validate([
                    'pickup_address'     => ['required', 'in:'.$ids],

                    'order_no_one'       => ['required', 'string', 'max:255'],
                    'name_one'           => ['required', 'string', 'max:255'],
                    'postal_code_one'    => ['required', 'regex:/[0-9]/'],
                    'address_one'        => ['required', 'string', 'max:255'],
                    'unit_no_one'        => ['required', 'string', 'max:255'],
                    'phone_one'          => ['required', 'regex:/[0-9]/'],
                    'longitude_one'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_one'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'order_no_two'       => ['required', 'string', 'max:255'],
                    'name_two'           => ['required', 'string', 'max:255'],
                    'postal_code_two'    => ['required', 'regex:/[0-9]/'],
                    'address_two'        => ['required', 'string', 'max:255'],
                    'unit_no_two'        => ['required', 'string', 'max:255'],
                    'phone_two'          => ['required', 'regex:/[0-9]/'],
                    'longitude_two'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_two'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'delivery_date'      => ['required', 'date'],
                    'delivery_time'      => ['required', 'date_format:H:i'],
                    'fee'                => ['required', 'numeric'],
                ]);

                $postal_codes = [
                    'postal_code_one'    => $request->postal_code_one,
                    'postal_code_two'    => $request->postal_code_two,
                ];
            }

            else if (!$request->has_address_two && $request->has_address_three) {
                $request->validate([
                    'pickup_address'     => ['required', 'in:'.$ids],

                    'order_no_one'       => ['required', 'string', 'max:255'],
                    'name_one'           => ['required', 'string', 'max:255'],
                    'postal_code_one'    => ['required', 'regex:/[0-9]/'],
                    'address_one'        => ['required', 'string', 'max:255'],
                    'unit_no_one'        => ['required', 'string', 'max:255'],
                    'phone_one'          => ['required', 'regex:/[0-9]/'],
                    'longitude_one'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_one'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'order_no_three'     => ['required', 'string', 'max:255'],
                    'name_three'         => ['required', 'string', 'max:255'],
                    'postal_code_three'  => ['required', 'regex:/[0-9]/'],
                    'address_three'      => ['required', 'string', 'max:255'],
                    'unit_no_three'      => ['required', 'string', 'max:255'],
                    'phone_three'        => ['required', 'regex:/[0-9]/'],
                    'longitude_three'    => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_three'     => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'delivery_date'      => ['required'],
                    'delivery_time'      => ['required', 'date_format:H:i'],
                    'fee'                => ['required', 'numeric'],
                ]);

                $postal_codes = [
                    'postal_code_one'    => $request->postal_code_one,
                    'postal_code_three'  => $request->postal_code_three,  
                ];
            }

            else {
                $request->validate([
                    'pickup_address'     => ['required', 'in:'.$ids],

                    'order_no_one'       => ['required', 'string', 'max:255'],
                    'name_one'           => ['required', 'string', 'max:255'],
                    'postal_code_one'    => ['required', 'regex:/[0-9]/'],
                    'address_one'        => ['required', 'string', 'max:255'],
                    'unit_no_one'        => ['required', 'string', 'max:255'],
                    'phone_one'          => ['required', 'regex:/[0-9]/'],
                    'longitude_one'      => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                    'latitude_one'       => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],

                    'delivery_date'      => ['required'],
                    'delivery_time'      => ['required', 'date_format:H:i'],
                    'fee'                => ['required', 'numeric'],
                ]);

                $postal_codes = [
                    'postal_code_one'    => $request->postal_code_one,
                ];
            }
            
            $minutes = Config::where('name', 'time_limit')->first()->value;
            $converted_delivery_date = strtr($request->delivery_date, '/', '-');
            $delivery_datetime = strtotime($converted_delivery_date.' '.$request->delivery_time.' GMT');
            $get_raw_datetime = date('Y-m-d H:i', strtotime('+ '.$minutes.' minutes'));
            $current_datetime = strtotime($get_raw_datetime.' GMT');
            
            if ($current_datetime > $delivery_datetime) {
                return redirect()->back()->withInput(Input::all())->with('error', 'Delivery date and time cannot be under '.$minutes.' minute(s) from the current date and time.');
            }

            $validate = $this->uniqueTokenValidator($request->unique_token);
            if ($validate == false) {
                return redirect()->route('delivery.create')->with('error', 'Please do not click on the submit button more than once.'); 
            }

            $postal_count = count($postal_codes);
            $total_fee = 0;
            $i = 1;

            foreach ($postal_codes as $code) {
                $url = "https://developers.onemap.sg/commonapi/search?searchVal=".$code."&returnGeom=Y&getAddrDetails=Y&pageNum=1";
                $curl_init = curl_init($url);
                curl_setopt($curl_init, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl_init, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl_init, CURLOPT_SSL_VERIFYPEER, 0);
		        $curl_exec = curl_exec($curl_init);
                $curl_getinfo = curl_getinfo($curl_init, CURLINFO_HTTP_CODE);
                curl_close($curl_init);

                $address = json_decode($curl_exec)->results;

                $google_latlong_reponse = $this->googleGetLatLong($address[0]->BLK_NO.' '.$address[0]->ROAD_NAME);

                if(!empty($google_latlong_reponse->results)) {
                    $address[0]->LATITUDE = $google_latlong_reponse->results[0]->geometry->location->lat;
                    $address[0]->LONGITUDE = $google_latlong_reponse->results[0]->geometry->location->lng;
                }

                $fee = $this->calculateFee($request->pickup_address, $address[0]);
                $total_fee = $total_fee + $fee;
                $individual_fee[$i] = $fee;
                $i = $i +1;
            }
            if ($total_fee != $request->fee) {
                return redirect()->back()->with('error', 'One or more value were not valid.');
            }
            
            $pickup_address = Address::find($request->pickup_address);

            if ($request->has_address_two && $request->has_address_three) {
                $deliveries = array('1','2','3');
            }
            elseif ($request->has_address_two && !$request->has_address_three) {
                $deliveries = array('1','2');
            }
            elseif (!$request->has_address_two && $request->has_address_three) {
                $deliveries = array('1','3');
            }
            else {
                $deliveries = array('1');
            }  

            $count = count($deliveries);
            $saved_deliveries = [];

            foreach ($deliveries as $delivery) {
                $create_delivery[$delivery] = new Delivery;
                $create_delivery[$delivery]->poster_id = auth()->user()->id;
                $create_delivery[$delivery]->pickup_address_id = $request->pickup_address;
                $create_delivery[$delivery]->pickup_address = 'Name: '.$pickup_address->name.'^Contact: '.$pickup_address->phone.'^Address: '.$pickup_address->address.' #'.$pickup_address->unit_no.' S'.$pickup_address->postal_code;
                $create_delivery[$delivery]->delivery_date = $request->delivery_date;
                $create_delivery[$delivery]->delivery_time = $request->delivery_time;
                if($delivery == 1) {
                    $create_delivery[$delivery]->fee = $individual_fee[1];
                    $create_delivery[$delivery]->total_fee = $individual_fee[1];
                    $create_delivery[$delivery]->delivery_address = $request->address_one;
                    $create_delivery[$delivery]->status = DeliveryStatus::PENDING;
                    $create_delivery[$delivery]->longitude = $request->longitude_one;
                    $create_delivery[$delivery]->latitude = $request->latitude_one;
                    $create_delivery[$delivery]->order_no = $request->order_no_one;
                    $create_delivery[$delivery]->contact = $request->phone_one;
                    $create_delivery[$delivery]->name = $request->name_one;
                    $create_delivery[$delivery]->unit_no = $request->unit_no_one;
                    $create_delivery[$delivery]->postal_code = $request->postal_code_one;

                }
                elseif($delivery == 2) {
                    $create_delivery[$delivery]->fee = $individual_fee[2];
                    $create_delivery[$delivery]->total_fee = $individual_fee[2];
                    $create_delivery[$delivery]->delivery_address = $request->address_two;  
                    $create_delivery[$delivery]->status = DeliveryStatus::PENDING;
                    $create_delivery[$delivery]->longitude = $request->longitude_two;
                    $create_delivery[$delivery]->latitude = $request->latitude_two;
                    $create_delivery[$delivery]->order_no = $request->order_no_two;
                    $create_delivery[$delivery]->contact = $request->phone_two;
                    $create_delivery[$delivery]->name = $request->name_two;
                    $create_delivery[$delivery]->unit_no = $request->unit_no_two;
                    $create_delivery[$delivery]->postal_code = $request->postal_code_two;
                }
                else {
                    $create_delivery[$delivery]->fee = $individual_fee[$count];
                    $create_delivery[$delivery]->total_fee = $individual_fee[$count];
                    $create_delivery[$delivery]->delivery_address = $request->address_three;
                    $create_delivery[$delivery]->status = DeliveryStatus::PENDING;
                    $create_delivery[$delivery]->longitude = $request->longitude_three;
                    $create_delivery[$delivery]->latitude = $request->latitude_three;
                    $create_delivery[$delivery]->order_no = $request->order_no_three;
                    $create_delivery[$delivery]->contact = $request->phone_three;
                    $create_delivery[$delivery]->name = $request->name_three;
                    $create_delivery[$delivery]->unit_no = $request->unit_no_three;
                    $create_delivery[$delivery]->postal_code = $request->postal_code_three;
                }

                $create_delivery[$delivery]->save();
                
                $created_delivery[$delivery] = [
                    $create_delivery[$delivery],
                    'delivery_key' => $delivery,
                ];
                array_push($saved_deliveries, $created_delivery[$delivery]);   
            }
            $total_addresses = 1;

            if ($request->has_address_two) {
                $total_addresses = $total_addresses + 1;
            }

            if ($request->has_address_three) {
                $total_addresses = $total_addresses + 1;
            }

            $pickup_longitude = $pickup_address->longitude;
            $pickup_latitude = $pickup_address->latitude;

            $mode = Config::where('name', 'mode')->first()->value;

            if ($mode == 'production') {
                $tookan_create = new Tookan;
                $response = $tookan_create->createTask($total_addresses, $saved_deliveries, $pickup_longitude, $pickup_latitude);

                $has_error = [];

                foreach($response as $result) {
                    if($result['status'] != 200) {
                        if ($result['step'] == 1) {
                            $create_delivery[$result['delivery_key']]->post_fail = 1;
                        }
                        else {
                            $create_delivery[$result['delivery_key']]->detail_fail = 1;
                        }
                        $errors = [
                            'api_http_code' => $result['status'],
                            'message' => $result['message'],
                            'delivery_data' => $create_delivery[$result['delivery_key']],
                        ];
                        array_push($has_error, $errors); 
                    }
                    else {
                        $create_delivery[$result['delivery_key']]->status = $result['data']['job_status'];
                        $create_delivery[$result['delivery_key']]->tracking_link = $result['data']['tracking_link'];
                        $create_delivery[$result['delivery_key']]->pickup_id = $result['data']['pickup_id'];
                        $create_delivery[$result['delivery_key']]->delivery_id = $result['data']['delivery_id'];      
                    }

                    $create_delivery[$result['delivery_key']]->save();
                }

                if($has_error == []) {
                    return redirect()->route('delivery.index')->with('success', 'Delivery added to this system and Tookan App.'); 
                }

                else {
                    $admin = Config::where('name', 'admin_email')->first()->value;
                    $user = User::find(auth()->user()->id);
                    Mail::to($admin)->send(new ErrorMail($has_error, $user, $pickup_address));

                    return redirect()->route('delivery.index')->with('error', 'One or more delivery has been added to this system but failed in either task posting to or retrieving the task\'s status from Tookan App. More information have been emailed to DRIVR\'s Admin.');
                }
            }

            else {
                return redirect()->route('delivery.index')->with('success', 'Delivery added to this system.'); 
            }

                
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        }
    }

    public function deliveryEditForm($id)
    {
        $unique_token = $this->uniqueToken();
        $delivery = Delivery::find($id);
        $deliveryStatus = DeliveryStatus::getArray();
        $options = Option::all();
        $address = [];
        $pickup_address = $delivery->pickup_address;
        $pickup_address_array = explode('^', $pickup_address);
        foreach ($pickup_address_array as $array) {
            if ($array != ' ') {
                $value = explode(': ', $array);
                $data = ['data' => $value[1]];
                array_push($address, $data);
            }
        }

        if($delivery->option_one != 0) {
            $get_value_one = explode('_', $delivery->option_one); 
            $delivery->value_one = $get_value_one[1];
            $delivery->get_option_one = $get_value_one[0];
        }

        if($delivery->option_two != 0) {
            $get_value_two = explode('_', $delivery->option_two); 
            $delivery->value_two = $get_value_two[1];
            $delivery->get_option_two = $get_value_two[0];
        }

        if($delivery->option_three != 0) {
            $get_value_three = explode('_', $delivery->option_three); 
            $delivery->value_three = $get_value_three[1];
            $delivery->get_option_three = $get_value_three[0];
        }

        if($delivery->option_four != 0) {
            $get_value_four = explode('_', $delivery->option_four); 
            $delivery->value_four = $get_value_four[1];
            $delivery->get_option_four = $get_value_four[0];
        }

        if (auth()->user()->is_admin) {
            return view('vendor.adminlte.delivery.edit', compact('address', 'delivery', 'unique_token', 'deliveryStatus', 'options'));
        }
        else {
            return redirect('/address')->with('error', 'Action not allowed.');
        }
    }

    public function deliveryUpdate(Request $request, $id)
    {
        $ids_array = [];
        $options = Option::all();
        foreach ($options as $option) {
            array_push($ids_array, $option->id);
        }

        $ids = implode(',', $ids_array);

        if (auth()->user()->is_admin) {
            if($request->has_add_one && $request->has_add_two && $request->has_add_three && $request->has_add_four) {
                $request->validate([
                    'status'            => ['required'],
                    'option_one'        => ['required','in:'.$ids],
                    'option_two'        => ['required','in:'.$ids],
                    'option_three'      => ['required','in:'.$ids],
                    'option_four'       => ['required','in:'.$ids],
                    'value_one'         => ['required','numeric','between:0.00,999.99'],
                    'value_two'         => ['required','numeric','between:0.00,999.99'],
                    'value_three'       => ['required','numeric','between:0.00,999.99'],
                    'value_four'        => ['required','numeric','between:0.00,999.99'],
                ]);
            }
            elseif($request->has_add_one && $request->has_add_two && $request->has_add_three && !$request->has_add_four) {
                $request->validate([
                    'status'            => ['required'],
                    'option_one'        => ['required','in:'.$ids],
                    'option_two'        => ['required','in:'.$ids],
                    'option_three'      => ['required','in:'.$ids],
                    'value_one'         => ['required','numeric','between:0.00,999.99'],
                    'value_two'         => ['required','numeric','between:0.00,999.99'],
                    'value_three'       => ['required','numeric','between:0.00,999.99'],
                ]);
            }
            elseif($request->has_add_one && $request->has_add_two && !$request->has_add_three && !$request->has_add_four) {
                $request->validate([
                    'status'            => ['required'],
                    'option_one'        => ['required','in:'.$ids],
                    'option_two'        => ['required','in:'.$ids],
                    'value_one'         => ['required','numeric','between:0.00,999.99'],
                    'value_two'         => ['required','numeric','between:0.00,999.99'],
                ]);
            }
            elseif($request->has_add_one && !$request->has_add_two && !$request->has_add_three && !$request->has_add_four) {
                $request->validate([
                    'status'            => ['required'],
                    'option_one'        => ['required','in:'.$ids],
                    'value_one'         => ['required','numeric','between:0.00,999.99'],
                ]);
            }
            elseif(!$request->has_add_one && !$request->has_add_two && !$request->has_add_three && !$request->has_add_four) {
                $request->validate([
                    'status'            => ['required'],
                ]);
            }
            else {
                return redirect()->route('delivery.edit', $id)->with('error', 'Unauthorized action detected.'); 
            }

            $validate = $this->uniqueTokenValidator($request->unique_token);
            if ($validate == false) {
                return redirect()->route('delivery.edit', $id)->with('error', 'Please do not click on the submit button more than once.'); 
            }

            $delivery = Delivery::find($id);
            $total_fee = $delivery->fee;
            $delivery->status = $request->status;
            if($request->has_add_one) {
                $delivery->option_one = $request->option_one.'_'.$request->value_one;
                $total_fee = $total_fee + $request->value_one;
            }
            else {
                $delivery->option_one = 0;
            }
            if($request->has_add_two){
                $delivery->option_two = $request->option_two.'_'.$request->value_two;
                $total_fee = $total_fee + $request->value_two;
            }
            else {
                $delivery->option_two = 0;
            }
            if($request->has_add_three){
                $delivery->option_three = $request->option_three.'_'.$request->value_three;
                $total_fee = $total_fee + $request->value_three;
            }
            else {
                $delivery->option_three = 0;
            }
            if($request->has_add_four){
                $delivery->option_four = $request->option_four.'_'.$request->value_four;
                $total_fee = $total_fee + $request->value_four;
            }
            else {
                $delivery->option_four = 0;
            }
            $delivery->total_fee = $total_fee;
            $delivery->save();
                return redirect()->route('delivery.index')->with('success', 'Delivery updated.');
        }
        else {
            return redirect('/delivery')->with('error', 'Action not allowed.');
        }
    }

    public function deliveryCancel($id)
    {   
        if (!auth()->user()->is_admin) {
            $delivery = Delivery::find($id);
            $address = Address::find($delivery->pickup_address_id);
            $has_error = [];

            if ($delivery->poster_id == auth()->user()->id) {
                $mode = Config::where('name', 'mode')->first()->value;

                if ($mode == 'production') {
                    if(!is_null($delivery->pickup_id) && !is_null($delivery->delivery_id)) {
                        $pickup_id = $delivery->pickup_id;
                        $delivery_id = $delivery->delivery_id;
                        $address = Address::find($delivery->pickup_address_id);
                        $status = DeliveryStatus::CANCEL;
                        $tookan_cancel = new Tookan;
                        $pickup = $tookan_cancel->updateStatus($pickup_id, $status);
                        if($pickup['status'] != 200) {
                            $errors = [
                                'api_http_code' => $pickup['status'],
                                'message' => $pickup['message'],
                                'delivery_data' => $delivery,
                            ];
                            array_push($has_error, $errors);
                        }
                        else {
                            $response = $tookan_cancel->updateStatus($delivery_id, $status);
                            if ($response['status'] != 200) {
                                $errors = [
                                    'api_http_code' => $response['status'],
                                    'message' => $response['message'],
                                    'delivery_data' => $delivery,
                                ];
                                array_push($has_error, $errors); 
                            }
                        }
                    }
                    if($has_error != []) {
                        $admin = Config::where('name', 'admin_email')->first()->value;
                        $user = User::find(auth()->user()->id);
                        Mail::to($admin)->send(new ErrorMail($has_error, $user, $address));

                        Session::flash('error', 'Failed to cancel the task on Tookan App. More information have been emailed to DRIVR\'s Admin.');
                        return response()->json(['success'=>'error']);
                    }
                    else {
                        $delivery->status = DeliveryStatus::CANCEL;
                        $delivery->save();
                        $this->sendNotification($delivery);

                        Session::flash('success', 'Delivery has been cancelled on both DRIVR\'s system and Tookan App.');
                        return response()->json(['success'=>'success']);
                    }
                }
                else {
                    $delivery->status = DeliveryStatus::CANCEL;
                    $delivery->save();

                    Session::flash('success', 'Delivery has been cancelled on both DRIVR\'s system.');
                    return response()->json(['success'=>'success']);
                }
                
            }
            else {
                return redirect('/delivery')->with('error', 'You are not authorized to do that action.');
            }
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        }
    }

    public function deliveryRepostTookan($id)
    {   
        if (!auth()->user()->is_admin) {
            $delivery = Delivery::find($id);
            $address = Address::find($delivery->pickup_address_id);
            $tookan_create = new Tookan;
            $response = $tookan_create->repost($delivery, $address);
            $has_error = [];

            if ($response[0]['status'] != 200) {
                if ($response[0]['step'] == 1) {
                        $delivery->post_fail = 1;
                        $delivery->detail_fail = 0;
                }
                else {
                    $delivery->post_fail = 0;
                    $delivery->detail_fail = 1;
                }
                $errors = [
                    'api_http_code' => $response[0]['status'],
                    'message' => $response[0]['message'],
                    'delivery_data' => $delivery,
                ];
                array_push($has_error, $errors); 
            }
            else {
                $delivery->post_fail = 0;
                $delivery->detail_fail = 0;
                $delivery->status = $response[0]['data']['job_status'];
                $delivery->tracking_link = $response[0]['data']['tracking_link'];   
            }

            $delivery->save();
            if($has_error == []) {
                return redirect()->route('delivery.index')->with('success', 'Task have been posted to Tookan App successfully.'); 
            }

            else {
                $admin = Config::where('name', 'admin_email')->first()->value;
                $user = User::find(auth()->user()->id);
                Mail::to($admin)->send(new ErrorMail($has_error, $user, $address));

                if ($response[0]['step'] == 1) {
                    return redirect()->route('delivery.index')->with('error', 'Failed again in task posting to Tookan App. More information have been emailed to DRIVR\'s Admin.');
                }
                else {
                    return redirect()->route('delivery.index')->with('error', 'Failed in getting the task\'s status from Tookan App. More information have been emailed to DRIVR\'s Admin.');
                }
            }
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        }
    }

    public function deliveryGetDetailTookan($id)
    {   
        if (!auth()->user()->is_admin) {
            $delivery = Delivery::find($id);
            $pickup_id = $delivery->pickup_id;
            $delivery_id = $delivery->delivery_id;
            $address = Address::find($delivery->pickup_address_id);
            $tookan_get = new Tookan;
            $response = $tookan_get->getTaskDetails($pickup_id, $delivery_id);
            $has_error = [];
            if ($response['status'] != 200) {
                $delivery->post_fail = 0;
                $delivery->detail_fail = 1;
                $errors = [
                    'api_http_code' => $response['status'],
                    'message' => $response['message'],
                    'delivery_data' => $delivery,
                ];
                array_push($has_error, $errors); 
            }
            else {
                $delivery->post_fail = 0;
                $delivery->detail_fail = 0;
                $delivery->status = $response['data']['job_status'];
                $delivery->tracking_link = $response['data']['tracking_link'];   
            }

            $delivery->save();
            if($has_error == []) {
                return redirect()->route('delivery.index')->with('success', 'Task\'s status has been retrieved successfully from Tookan App.'); 
            }

            else {
                $admin = Config::where('name', 'admin_email')->first()->value;
                $user = User::find(auth()->user()->id);
                Mail::to($admin)->send(new ErrorMail($has_error, $user, $address));

                return redirect()->route('delivery.index')->with('error', 'Failed again in getting the task\'s status from Tookan App. More information have been emailed to DRIVR\'s Admin.');
            }
        }
        else {
            return redirect('/delivery')->with('error', 'Admin is not allowed.');
        }
    }


    public function sendNotification($data) {
        $user = User::find($data->poster_id);

        $pickup_locations = explode('^', $data->pickup_address);
        $pickup_array = [];
        foreach($pickup_locations as $pickup_location) {
            $pickup_loc = ['location' => $pickup_location];
            array_push($pickup_array, $pickup_loc);
        }
        $data->pickup_array = $pickup_array;

        $admin = Config::where('name', 'admin_email')->first()->value;
        Mail::to($admin)->send(new NotificationMail($data, $user));
    }

    public function configList()
    {
        return view('vendor.adminlte.admin.config.index');
    }

    public function configDatatable()
    {
        $configs = Config::all();
        return Datatables::of($configs)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = "<a href='".route('config.edit', $row->id)."' class='btn btn-primary'>Edit</a>";
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function configEditForm($id)
    {   
        $unique_token = $this->uniqueToken();
        $config = Config::find($id);
        return view('vendor.adminlte.admin.config.edit', compact('config', 'unique_token'));
    }

    public function configUpdate(Request $request, $id)
    {       
        
        
        if($request->type == 'time') {
            $request->validate([
                'value' => 'required|numeric|min:60',
            ]);
        }
        else if($request->type == 'mode' || $request->type == 'api') {
            $request->validate([
                'value' => 'required|in:1,0',
            ]);
        }
        else {
           $request->validate([
                'value' => 'required',
            ]); 
        }
        
        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('config.edit')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $config = Config::find($id);
        if($request->type == 'mode') {

            if($request->value == 0) {
                $config->value = "development";
            }
            else {
                $config->value = "production";
            }
        }
        if($request->type == 'api') {

            if($request->value == 0) {
                $config->value = "OneMap SG";
            }
            else {
                $config->value = "Google Map";
            }
        }
        else {
            $config->value = $request->value;
        }
        $config->save();

        return redirect()->route('config.index')->with('success', 'Config updated.');
    }

    public function optionList()
    {
        return view('vendor.adminlte.admin.option.index');
    }

    public function optionDatatable()
    {
        $options = Option::all();
        return Datatables::of($options)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = "<a href='".route('option.edit', $row->id)."' class='btn btn-primary'>Edit</a>";
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function optionCreateForm()
    {   
        $unique_token = $this->uniqueToken();
        return view('vendor.adminlte.admin.option.create', compact('unique_token'));
    }

    public function optionStore(Request $request)
    {       
        $request->validate([
            'name' => 'required|string',
        ]);
        
        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('option.create')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $option = new Option;
        $option->name = $request->name;
        $option->save();

        return redirect()->route('option.index')->with('success', 'Option created.');
    }

    public function optionEditForm($id)
    {   
        $unique_token = $this->uniqueToken();
        $option = Option::find($id);
        return view('vendor.adminlte.admin.option.edit', compact('option', 'unique_token'));
    }

    public function optionUpdate(Request $request, $id)
    {       
        $request->validate([
            'name' => 'required|string',
        ]);
        
        $validate = $this->uniqueTokenValidator($request->unique_token);
        if ($validate == false) {
            return redirect()->route('option.edit')->with('error', 'Please do not click on the submit button more than once.'); 
        }

        $option = Option::find($id);
        $option->name = $request->name;
        $option->save();

        return redirect()->route('option.index')->with('success', 'Option updated.');
    }
}
