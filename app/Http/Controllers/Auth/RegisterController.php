<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\RegistrationMail;
use App\Mail\AdminNotifyMail;
use App\Models\Config;
use Session;
use Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $random_string = mt_rand().'+'.uniqid().'+'.mt_rand(1000,9999);
        $hashed_string = hash('sha256', $random_string);
        Session::put('unique_token', $hashed_string);
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'regex:/^[\w-]*$/', 'min:8', 'max:16', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $email = Config::where('name', 'admin_email')->first()->value;
        Mail::to($data['email'])->send(new RegistrationMail($data));
        Mail::to($email)->send(new AdminNotifyMail($data));

        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        if ($request->unique_token != Session::get('unique_token')) {
            return redirect('/register')->with('error', 'Please do not click on register button more than once.');
        }

        $random_string = mt_rand().'+'.uniqid().'+'.mt_rand(1000,9999);
        $hashed_string = hash('sha256', $random_string);
        Session::put('unique_token', $hashed_string);
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return redirect('login')->with('success', 'Your account have been registered successfully!. <br/>Please wait for the admin to approve your account before you can login.');
    }
}
