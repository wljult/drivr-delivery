<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Enums\DeliveryStatus;

class ErrorMail extends Mailable
{
    use Queueable, SerializesModels;
    public $has_error;
    public $user;
    public $pickup;

    /**
     * Create a new message instance.
     *
     * @return void
     */
        public function __construct($has_error, $user, $pickup)
    {
        $this->has_error = $has_error;
        $this->user = $user;
        $this->pickup = $pickup;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        foreach($this->has_error as $error) {
            $error['delivery_data']->status_text = DeliveryStatus::getString($error['delivery_data']->status);
        }
        return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'))
        ->subject('Error Report')
        ->view('emails.error');
    }
}
