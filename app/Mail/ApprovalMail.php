<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovalMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $status;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $status, $email = '')
    {
        $this->data = $data;
        $this->status = $status;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'))
        ->subject('Your account approval\'s status')
        ->view('emails.approval');
    }
}
