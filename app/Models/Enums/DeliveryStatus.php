<?php

namespace App\Models\Enums;

final class DeliveryStatus
{
	const ASSIGNED = 0;
	const STARTED = 1;
	const SUCCESSFUL = 2;
	const FAILED = 3;
	const INPROGRESS = 4;
	const UNASSIGNED = 6;
	const ACCEPTED = 7;
	const DECLINE = 8;
	const CANCEL = 9;
	const DELETED = 10;
	const PENDING = 11;

	public static function getList()
	{
		return [
			static::ASSIGNED, static::STARTED,
			static::SUCCESSFUL, static::FAILED,
			static::INPROGRESS, static::UNASSIGNED, 
			static::ACCEPTED, static::DECLINE,
			static::CANCEL, static::DELETED,
			static::PENDING,
		];
	}

	public static function getArray($select2 = false)
	{
		$result = [];
		foreach (self::getList() as $arr) {
			if ($select2) {
				$result[] = ['id' => $arr, 'text' => self::getString($arr)];
			} else {
				$result[$arr] = self::getString($arr);
			}
		}
		return $result;
	}

	public static function getString($val)
	{
		switch ($val) {
			case static::ASSIGNED:
				return 'Assigned';
			case static::STARTED:
				return 'Started';
			case static::SUCCESSFUL:
				return 'Successful';
			case static::FAILED:
				return 'Failed';
			case static::INPROGRESS:
				return 'In Progress';
			case static::UNASSIGNED:
				return 'Unassigned';
			case static::ACCEPTED:
				return 'Accepted';
			case static::DECLINE:
				return 'Decline';
			case static::CANCEL:
				return 'Cancel';
			case static::DELETED:
				return 'Deleted';
			case static::PENDING:
				return 'Pending';
		}
	}
}

?>