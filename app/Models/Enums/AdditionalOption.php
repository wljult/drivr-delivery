<?php

namespace App\Models\Enums;

final class AdditionalOption
{
	const ERP = 1;
	const RESTRICTED = 2;
	const WAITING = 3;
	const OTHER = 4;
	

	public static function getList()
	{
		return [
			static::ERP, static::RESTRICTED,
			static::WAITING, static::OTHER,
		];
	}

	public static function getArray($select2 = false)
	{
		$result = [];
		foreach (self::getList() as $arr) {
			if ($select2) {
				$result[] = ['id' => $arr, 'text' => self::getString($arr)];
			} else {
				$result[$arr] = self::getString($arr);
			}
		}
		return $result;
	}

	public static function getString($val)
	{
		switch ($val) {
			case static::ERP:
				return 'ERP';
			case static::RESTRICTED:
				return 'Restricted Area';
			case static::WAITING:
				return 'Waiting/Late Fee';
			case static::OTHER:
				return 'Others';
		}
	}
}

?>