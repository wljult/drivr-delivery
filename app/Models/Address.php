<?php

namespace App\Models;

use App\Models\Delivery;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function delivery()
    {
        return $this->hasMany(Delivery::class);
    }
}
