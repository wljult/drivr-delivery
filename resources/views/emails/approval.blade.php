<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Your account approval's status</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff !important;
                color: #636b6f !important;
                font-family: 'Nunito', sans-serif !important;
                font-weight: 200 !important;
                height: 100% !important;
                margin: 0 !important;
            }

            .full-height {
                height: 100% !important;
            }

            .flex-left {
                align-items: left !important;
                display: flex !important;
                justify-content: left !important;
            }

            .position-ref {
                position: relative !important;
            }

            .top-right {
                position: absolute !important;
                right: 10px !important;
                top: 18px !important;
            }

            .content {
                text-align: left !important;
            }

            .title {
                font-size: 36px !important;
            }

            .links > a {
                color: #636b6f !important;
                padding: 0 25px !important;
                font-size: 13px !important;
                font-weight: 600 !important;
                letter-spacing: .1rem !important;
                text-decoration: none !important;
                text-transform: uppercase !important;
            }

            .m-b-md {
                margin-bottom: 30px !important;
            }

            p.detail>b {
                font-size: 24px;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-left position-ref full-height">
            <div class="content">

                <div class="title m-b-md">
                    Hi {{$data->name}},
                </div>
                <div class="em_div" style="white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </div>
                <div class="links">
                @if ($status == 'approved')
                    <p>We are happy to tell you that your account submission has been approved!</p>
                    <p>You can now start logging in and use our services.</p>
                    <br/>
                    <p>Best regards,</p>
                    <br/>
                    <p>DRIVR's Team</p>
                @else
                    <p>We are sorry to tell you that your account submission has been denied.</p>
                    <p>We are unable to approve your submission at the moment.</p>
                    <p>Please contact us at {{$email}} for more information or if you decided to apply for re-approval.</p>
                    <br/>
                    <p>Best regards,</p>
                    <br/>
                    <p>DRIVR's Team</p>
                @endif
                 </div>

            </div>
        </div>
    </body>
</html>
