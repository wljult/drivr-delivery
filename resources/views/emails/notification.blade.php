<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Cancellation Notice</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff !important;
                color: #636b6f !important;
                font-family: 'Nunito', sans-serif !important;
                font-weight: 200 !important;
                height: 100% !important;
                margin: 0 !important;
            }

            .full-height {
                height: 100% !important;
            }

            .flex-left {
                align-items: left !important;
                display: flex !important;
                justify-content: left !important;
            }

            .position-ref {
                position: relative !important;
            }

            .top-right {
                position: absolute !important;
                right: 10px !important;
                top: 18px !important;
            }

            .content {
                text-align: left !important;
            }

            .title {
                font-size: 36px !important;
            }

            .links > a {
                color: #636b6f !important;
                padding: 0 25px !important;
                font-size: 13px !important;
                font-weight: 600 !important;
                letter-spacing: .1rem !important;
                text-decoration: none !important;
                text-transform: uppercase !important;
            }

            .m-b-md {
                margin-bottom: 30px !important;
            }

            p.detail>b {
                font-size: 24px;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-left position-ref full-height">
            <div class="content">

                <div class="title m-b-md">
                    Hi Admin,
                </div>
            	<div class="em_div" style="white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">
                	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </div>
                <div class="links">
                	<br>
                    <p>A delivery job requested by {{$user->name}} with email address of {{$user->email}} have been cancelled by the poster.</p>
                    <p class="detail"><b>DETAILS<b></p>
                    <p>Delivery date: {{date('d/m/Y', strtotime($data->delivery_date))}}</p>
                    <p>Delivery time: {{date('h:i A', strtotime($data->delivery_time))}}</p>
                    <p>Fee: SGD {{$data->fee}}</p>
                    <p>Status: <b>CANCELLED</b></p>
                    <p>From:</p>
                    <ul>
                        @foreach($data->pickup_array as $pickup) 
                            <li>{{$pickup['location']}}</li>
                        @endforeach
                    </ul>
                    <p>To:</p>
                    <ul>
                        <li>Name: {{$data->name}}</li>
                        <li>Contact: {{$data->contact}}</li>
                        <li>Address: {{$data->delivery_address}} #{{$data->unit_no}} S{{$data->postal_code}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
