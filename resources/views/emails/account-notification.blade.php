<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>New account registration notice</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff !important;
                color: #636b6f !important;
                font-family: 'Nunito', sans-serif !important;
                font-weight: 200 !important;
                height: 100% !important;
                margin: 0 !important;
            }

            .full-height {
                height: 100% !important;
            }

            .flex-left {
                align-items: left !important;
                display: flex !important;
                justify-content: left !important;
            }

            .position-ref {
                position: relative !important;
            }

            .top-right {
                position: absolute !important;
                right: 10px !important;
                top: 18px !important;
            }

            .content {
                text-align: left !important;
            }

            .title {
                font-size: 36px !important;
            }

            .links > a {
                color: #636b6f !important;
                padding: 0 25px !important;
                font-size: 13px !important;
                font-weight: 600 !important;
                letter-spacing: .1rem !important;
                text-decoration: none !important;
                text-transform: uppercase !important;
            }

            .m-b-md {
                margin-bottom: 30px !important;
            }

            p.detail>b {
                font-size: 24px;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-left position-ref full-height">
            <div class="content">

                <div class="title m-b-md">
                    Hi Admin,
                </div>
                <div class="em_div" style="white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </div>
                <div class="links">
                    <br>
                    <p>An user has registered under the name of {{$data['name']}} with email address of {{$data['email']}}.</p>
                    <p class="detail"><b>DETAILS<b></p>
                    <p>Name: {{$data['name']}}</p>
                    <p>Username: {{$data['username']}}</p>
                    <p>Email: {{$data['email']}}</p>
                    <br/>
                    <p>Please do review it as soon as possible.</p>
                    <br/> 
                    <p>Best regards,</p>
                    <br/>
                    <p>DRIVR's System</p>       
                </div>
            </div>
        </div>
    </body>
</html>
