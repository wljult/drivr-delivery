<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Error Report</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff !important;
                color: #636b6f !important;
                font-family: 'Nunito', sans-serif !important;
                font-weight: 200 !important;
                height: 100% !important;
                margin: 0 !important;
            }

            .full-height {
                height: 100% !important;
            }

            .flex-left {
                align-items: left !important;
                display: flex !important;
                justify-content: left !important;
            }

            .position-ref {
                position: relative !important;
            }

            .top-right {
                position: absolute !important;
                right: 10px !important;
                top: 18px !important;
            }

            .content {
                text-align: left !important;
            }

            .title {
                font-size: 36px !important;
            }

            .links > a {
                color: #636b6f !important;
                padding: 0 25px !important;
                font-size: 13px !important;
                font-weight: 600 !important;
                letter-spacing: .1rem !important;
                text-decoration: none !important;
                text-transform: uppercase !important;
            }

            .m-b-md {
                margin-bottom: 30px !important;
            }

            p.detail>b {
                font-size: 24px;
            }
            
        </style>
    </head>
    <body>
        <div class="flex-left position-ref full-height">
            <div class="content">

                <div class="title m-b-md">
                    Hi Admin,
                </div>
            	<div class="em_div" style="white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">
                	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </div>
                <div class="links">
                	<br>
                    <p>A delivery job requested by {{$user->name}} with email address of {{$user->email}} have encountered error with Tookan App's API.</p>
                    <p class="detail"><b>ERROR DETAILS<b></p>
                    <br/>
                    @foreach ($has_error as $error)
                        <p>TOOKAN API's HTTP code: {{$error['api_http_code']}}</p>
                        <p>TOOKAN API's Error message: {{$error['message']}}</p>
                        <br/>
                        <p>Delivery date: {{date('d/m/Y', strtotime($error['delivery_data']->delivery_date))}}</p>
                        <p>Delivery time: {{date('h:i A', strtotime($error['delivery_data']->delivery_time))}}</p>
                        <p>Fee: SGD {{$error['delivery_data']->fee}}</p>
                        <p>Status: <b>{{$error['delivery_data']->status_text}}</b></p>
                        <p>From:</p>
                        <ul>
                            <li>Name: {{$pickup->name}}</li>
                            <li>Contact: {{$pickup->phone}}</li>
                            <li>Address: {{$pickup->address}} #{{$pickup->unit_no}} S{{$pickup->postal_code}}</li>
                        </ul>
                        <p>To:</p>
                        <ul>
                            <li>Name: {{$error['delivery_data']->name}}</li>
                            <li>Contact: {{$error['delivery_data']->contact}}</li>
                            <li>Address: {{$error['delivery_data']->address}} #{{$error['delivery_data']->unit_no}} S{{$error['delivery_data']->postal_code}}</li>
                        </ul>
                        <br/>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>
