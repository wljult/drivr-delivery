@extends('adminlte::page')

@section('title', 'Option List')

@section('content_header')
    <h1>Option list</h1>
@endsection

@section('content')
    <div class="animated slideInUpTiny animation-duration-3">
        <div class="page-heading">
            <h2 class="title"><a href="{{ route('option.create') }}"><button type="button" class="btn btn-outline-primary drivr-add-button"><i class="drivr drivr-plus-square"></i> Add new </button></a></h2>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="gx-card">
                    <div class="gx-card-header">
                        <h3 class="card-heading"></h3>
                    </div>
                    <div class="gx-card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="table-responsive">                    
                            <table class="table table-striped" id="datatables-option">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Option name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('additional_js')
    <!--Datatables JQuery-->
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready(function () {
            const table = $('#datatables-option').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('option.datatable') }}",
                    data: function (d) {
                        d._token = '{{csrf_token()}}';
                    },
                    type: "POST",
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                "language": {
                  "emptyTable": "There is no option yet."
                },
            });
        });
    </script>
@endpush