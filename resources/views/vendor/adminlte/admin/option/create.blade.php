@extends('adminlte::page')

@section('title', 'Add Option')

@section('content_header')
    <h1>Add Option</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('option.store') }}" enctype="multipart/form-data">
	                        @method('POST') 
	                        @csrf

	                        <div class="form-group @error('name') has-error @enderror">
	                            <label for="name">Option's Name</label>
	                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
	                            @error('name')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                      	
	                        <input id="type" type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-option" onclick="disableButton()"  type="submit" class="btn btn-primary">Save</button>
	                            <a href="{{ route('option.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push('additional_js')
    <script>

    	function disableButton() {
	        var is_empty = false;
	        const type = $('#type').val();
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	            else if(type == 'time' && $(this).val() < 60){
	            	is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }
	</script>
@endpush