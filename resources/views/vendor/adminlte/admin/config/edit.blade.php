@extends('adminlte::page')

@section('title', 'Edit Config')

@section('content_header')
    <h1>Edit Config</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('config.update', $config->id) }}" enctype="multipart/form-data">
	                        @method('PUT') 
	                        @csrf

	                        <div class="form-group @error('name') has-error @enderror">
	                            <label for="name">Config name</label>
	                            <input id="name" type="text" class="form-control" value="{{$config->name}}" disabled>
	                            @error('name')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        @if($config->name == 'time_limit')
	                        <div class="form-group @error('value') has-error @enderror">
	                            <label for="value">Config value ( Minutes )</label>
	                            <input type="number" id="value" class="form-control" name="value" min="60" value="{{ old('value', $config->value) }}" required>
	                            <input id="type" type="hidden" name="type" value="time">
	                            @error('value')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        @elseif($config->name == 'mode')
	                        <div class="form-group @error('value') has-error @enderror">
	                            <label for="value">Config value</label>
	                             <select id="value" name="value" class="form-control">
	                             	<option value="0" @if($config->value == 'development') selected @endif>Development (Disable tookan posting)</option>
	                            	<option value="1" @if($config->value == 'production') selected @endif>Production</option>
	                            </select>
	                            <input id="type" type="hidden" name="type" value="mode">
	                            @error('value')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        @elseif($config->name == 'current_api')
	                        <div class="form-group @error('value') has-error @enderror">
	                            <label for="value">Config value</label>
	                             <select id="value" name="value" class="form-control">
	                             	<option value="0" @if($config->value == 'OneMap SG') selected @endif>OneMap SG</option>
	                            	<option value="1" @if($config->value == 'Google Map') selected @endif>Google Map</option>
	                            </select>
	                            <input id="type" type="hidden" name="type" value="api">
	                            @error('value')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        @else
	                        <div class="form-group @error('value') has-error @enderror">
	                            <label for="value">Config value</label>
	                            <textarea id="value"class="form-control" name="value" required>{{ old('value', $config->value) }}</textarea>
	                            <input type="hidden" name="type" value="other">
	                            @error('value')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        @endif
	                        <input id="type" type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-config" onclick="disableButton()"  type="submit" class="btn btn-primary">Save</button>
	                            <a href="{{ route('config.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push('additional_js')
    <script>

    	function disableButton() {
	        var is_empty = false;
	        const type = $('#type').val();
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	            else if(type == 'time' && $(this).val() < 60){
	            	is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }
	</script>
@endpush