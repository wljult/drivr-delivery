@extends('adminlte::page')

@section('title', 'User List')

@section('content_header')
    <h1>User list</h1>
@endsection

@section('content')
    <div class="animated slideInUpTiny animation-duration-3">
        <div class="page-heading">
            <h2 class="title"><a href="{{ route('user.create') }}"><button type="button" class="btn btn-outline-primary drivr-add-button"><i class="drivr drivr-plus-square"></i> Add new </button></a></h2>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="gx-card">
                    <div class="gx-card-header">
                        <h3 class="card-heading"></h3>
                    </div>
                    <div class="gx-card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="table-responsive">                    
                            <table class="table table-striped" id="datatables-user">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Approval</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('vendor.adminlte.includes.modal-delete')
@endsection

@push('additional_js')
    <!--Datatables JQuery-->
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script>
        function delete_user() {
            disableButton();
            const id = $('#modalDeleteID').val();
            const currentUrl = window.location.href;
            $.ajax({
                type: 'DELETE',
                url: "{{ route('user.destroy', ['user' => NULL]) }}/" + id,
                data: { user: id, _token: '{{csrf_token()}}' },
                success: function(data) {
                    window.location.href = currentUrl;
                }
            });
        }

        function disableButton() {
            var is_empty = false;
            $('input').each(function(){
                if($(this).val() == "" && $(this).prop('required')){
                    is_empty = true;
                }
            })
            if (is_empty == false) {
               $(".lds-dual-ring").show();
            }
        }

        $(document).ready(function () {
            const table = $('#datatables-user').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('user.datatable') }}",
                    data: function (d) {
                        d._token = '{{csrf_token()}}';
                    },
                    type: "POST",
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'username', name: 'username'},
                    {data: 'email', name: 'email'},
                    {data: 'role', name: 'role'},
                    {data: 'status', name: 'status'},
                    {data: 'approved', name: 'approved'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                "language": {
                  "emptyTable": "There is no user yet."
                },
            });
        });
    </script>
@endpush