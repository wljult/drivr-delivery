@extends('adminlte::page')

@section('title', 'Add New User')

@section('content_header')
    <h1>Add new user</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('user.store') }}" enctype="multipart/form-data">
	                        @method('POST') 
	                        @csrf
	                        <div class="form-group @error('name') has-error @enderror">
	                            <label for="name">Name</label>
	                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
	                            @error('name')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('username') has-error @enderror">
	                            <label for="username">Username</label>
	                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>
	                            @error('username')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('email') has-error @enderror">
	                            <label for="email">Email</label>
	                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required style="text-transform: lowercase;">
	                            @error('email')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

                            <div class="form-group @error('password') has-error @enderror">
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @error('password')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>

	                        <div class="checkbox">
	                        	<label>
	                        		<input type="hidden" name="is_admin" value="0" />
	                                <input type="checkbox" name="is_admin" value="1" @if(old('is_admin') == 1) checked @endif>
	                        		<strong>Admin</strong>
	                        	</label>
	                        </div>

	                        <div class="checkbox">
	                        	<label>
	                        		<input type="hidden" name="is_enabled" value="0" />
	                                <input type="checkbox" name="is_enabled" value="1" @if(old('is_enabled') == 1) checked @endif>
	                        		<strong>Enable user</strong>
	                        	</label>
	                        </div>

	                        <input type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-user" onclick="disableButton()"  type="submit" class="btn btn-primary">Save</button>
	                            <a href="{{ route('user.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection
@push('additional_js')
    <script>

    	function disableButton() {
	        var is_empty = false;
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }
	</script>
@endpush