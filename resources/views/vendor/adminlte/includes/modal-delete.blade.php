@push('modal')
<div id="modalDelete" class="modal fade delete_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="delete-form">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="modalDeleteID" value="">
                    <p>Are you sure you want to delete this <span id="modalDeleteSubject"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="modalDeleteConfirm">Confirm</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endpush

@push('additional_js')
    <script>
        $('#modalDelete').on('show.bs.modal', function (e) {
            var target = $(e.relatedTarget);
            $('#modalDeleteID').val(target.data('id'));
            $('#modalDeleteSubject').text(target.data('subject'));
            $('#modalDeleteConfirm').attr('onclick', target.data('function')+'()');
        });
    </script>
@endpush