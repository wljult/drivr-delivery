@push('modal')
<div id="modalCancel" class="modal fade cancel_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <form id="cancel-form">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Confirm Cancel</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="modalCancelID" value="">
                    <p>Are you sure you want to cancel this <span id="modalCancelSubject"></span>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="modalCancelConfirm">Confirm</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endpush

@push('additional_js')
    <script>
        $('#modalCancel').on('show.bs.modal', function (e) {
            var target = $(e.relatedTarget);
            $('#modalCancelID').val(target.data('id'));
            $('#modalCancelSubject').text(target.data('subject'));
            $('#modalCancelConfirm').attr('onclick', target.data('function')+'()');
        });
    </script>
@endpush