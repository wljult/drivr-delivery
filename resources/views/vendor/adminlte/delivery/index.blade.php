@extends('adminlte::page')

@section('title', 'Delivery List')

@section('content_header')
    <h1>Delivery list</h1>
@endsection

@section('content')
    <div class="animated slideInUpTiny animation-duration-3">
        <div class="page-heading">
            <h2 class="title">
                @if (!auth()->user()->is_admin)
                <a href="{{ route('delivery.create') }}">
                    <button type="button" class="btn btn-outline-primary drivr-add-button"><i class="drivr drivr-plus-square"></i> Add new </button>
                </a>
                @endif
            </h2>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="gx-card">
                    <div class="gx-card-header">
                        <h3 class="card-heading"></h3>
                    </div>
                    <div class="gx-card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="table-responsive">
                            <div class="form-group date-input-form">
                                <label class="date-input-label" for="start">&nbsp;Start Date:&nbsp;&nbsp;</label>
                                <input id="start" class="date-input form-control" type="date" name="start" value="">
                                <span class="line-break">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <label class="date-input-label" for="end">&nbsp;End Date:&nbsp;&nbsp;</label>
                                <input id="end" class="date-input form-control" type="date" name="end" value="">
                                <span class="break-1280"></span>
                                <span class="line-break status-break">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <label class="status-select-label" for="end">&nbsp;Status:&nbsp;&nbsp;</label>
                                <select id="status" name="status" class="status-select form-control">
                                        <option value="0" selected> --- </option>
                                    @foreach($deliveryStatus as $key => $status)
                                        @if($status == 'In Progress' || $status == 'Successful' || $status == 'Failed')
                                        <option value="{{ $key }}">{{ $status }}</option>
                                        @elseif($status == 'Unassigned')
                                        <option value="{{ $key }}">Pending</option>
                                        @elseif($status == 'Cancel')
                                        <option value="{{ $key }}">Cancelled</option>
                                        @endif
                                    @endforeach
                                </select>  
                                <a id="filter-button" class="btn btn-primary">Filter</a> 
                            </div>
                            <div id="date-range-error"></div>                  
                            <table class="table table-striped" id="datatables-delivery">
                                <thead>
                                    <tr>
                                        <th class="min-tablet-l">No</th>
                                        <th class="min-tablet-l">Poster's name</th>
                                        <th class="min-tablet-l">Pickup address</th>
                                        <th class="min-tablet-l">Delivery date</th>
                                        <th class="min-tablet-l">Delivery time</th>
                                        <th class="hidden-col">Fee in SGD</th>
                                        <th class="hidden-col">Order No</th>
                                        <th class="hidden-col">Name</th>
                                        <th class="min-tablet-l">Delivery Location</th>
                                        <th class="min-tablet-l">Status</th>
                                        <th class="min-tablet-l">Fee (SGD)</th>
                                        <th class="min-tablet-l">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('vendor.adminlte.includes.modal-cancel')
@endsection

@push('additional_js')
    <!--Datatables JQuery-->
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script>
        function disableButton() {
            var is_empty = false;
            $('input').each(function(){
                if($(this).val() == "" && $(this).prop('required')){
                    is_empty = true;
                }
            })
            if (is_empty == false) {
               $(".lds-dual-ring").show();
            }
        }

        $(document).ready(function () {
            var start = $('#start').val();
            var end = $('#end').val();
            var status = $('#status').val();

            load_data();

            $('#filter-button').on('click', function() {
                var start = Date.parse( $('#start').val());
                var end = Date.parse( $('#end').val());
                var status = $('#status').val();
                if (isNaN(start) == false && isNaN(end) == false) {
                    if (start > end) {
                    $('#date-range-error').text('Start date must not be more than end date.');
                    }
                    else if (start < 946684800000) {
                        $('#date-range-error').text('Start date must not under 01/01/2000.');
                    }
                    else if (end < 946684800000) {
                        $('#date-range-error').text('End date must not under 01/01/2000.');
                    }
                    else {
                        $('#datatables-delivery').DataTable().destroy();
                        load_data(start, end, status);
                    }
                }
                
                else if (isNaN(end) == false && isNaN(start)) {
                    $('#date-range-error').text('Invalid or empty start date detected.');
                }

                else if (isNaN(end) && isNaN(start) == false) {
                    $('#date-range-error').text('Invalid or empty end date detected.');
                }

                else {
                    $('#datatables-delivery').DataTable().destroy();
                     load_data('', '', status);    
                }
            });

            function load_data(from_date = '', to_date = '', status = 0) {
                var table = $('#datatables-delivery').DataTable({
                    responsive:{
                        breakpoints: [
                            { name: 'hidden-col',   width: 50 }
                        ],
                        details: {
                            display: $.fn.dataTable.Responsive.display.childRowImmediate,
                            renderer: function ( api, rowIdx, columns ) {
                                var data = $.map( columns, function ( col, i ) {
                                    if (col.title == "Order No" || col.title == 'Name' || col.title == "Delivery date" || col.title == "Delivery time" || col.title == "Status" || col.title == "Fee in SGD") {
                                        return col.hidden ?
                                        '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
                                            '<td>'+col.title+':&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'</td> '+
                                            '<td>'+col.data+'</td>'+
                                        '</tr>' :
                                        '';

                                    }
                                    else if ("{{auth()->user()->is_admin}}" == 0 && col.title == 'Action') {
                                        return col.hidden ?
                                        '<tr data-dt-row="'+col.rowIndex+'" data-dt-column="'+col.columnIndex+'">'+
                                            '<td>'+col.title+':&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'</td> '+
                                            '<td>'+col.data+'</td>'+
                                        '</tr>' :
                                        '';    
                                    }
                                    else {
                                        
                                    }
                                    
                                } ).join('');
                 
                                return data ?
                                    $('<table/>').append( data ) :
                                    false;
                            },
                        },
                    },
                    pageLength: 25,
                    processing: true,
                    serverSide: true,
                    order: [ [0, 'desc'] ],
                    ajax: {
                        url: "{{ route('delivery.datatable') }}",
                        data: {from_date:from_date, to_date:to_date, status_filter:status, _token:'{{csrf_token()}}'},
                        type: "POST",
                    },
                    columns: [
                        {data: 'DT_RowIndex', name: 'id'},
                        {data: 'poster_name', name: 'poster_name'},
                        {data: 'pickup_location', render: '[<br/><br/> ].location', name: 'pickup_location'},
                        {data: 'delivery_date', name: 'delivery_date'},
                        {data: 'delivery_time', name: 'delivery_time'},
                        {data: 'fee', name: 'fee'},
                        {data: 'order_no', name: 'order_no'},
                        {data: 'name', name: 'name'},
                        {data: 'delivery_location', render: '[<br/><br/> ].data', name: 'delivery_location'},
                        { "data": "status", "name": "status", render:
                            function ( data, type, row, meta ) {
                              return "<a class='"+data.class+"' href='"+data.url+"' target='_blank' rel='noopener noreferrer'><b>"+data.status+"</b></a>";
                            }
                        },
                        {data: 'fee', name: 'fee'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
                    "language": {
                      "emptyTable": "There is no delivery yet."
                    },
                });
            }
        });

        function cancel_delivery() {
            disableButton();
            const id = $('#modalCancelID').val();
            const currentUrl = window.location.href;
            $.ajax({
                type: 'GET',
                url: "{{ route('delivery.cancel', ['delivery' => NULL]) }}/" + id,
                data: { delivery: id, _token: '{{csrf_token()}}' },
                success: function(data) {
                    window.location.href = currentUrl;
                }
            });
        }
    </script>
@endpush