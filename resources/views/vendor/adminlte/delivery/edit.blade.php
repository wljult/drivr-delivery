@extends('adminlte::page')

@section('title', 'Delivery Form')

@section('content_header')
    <h1>Edit delivery</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('delivery.update', $delivery->id) }}" enctype="multipart/form-data">
	                        @method('POST') 
	                        @csrf

	                        <div class="form-group @error('delivery_date') has-error @enderror">
	                            <label for="delivery_date">Delivery date</label>
	                            <input id="delivery_date" type="date" class="form-control" name="delivery_date" value="{{$delivery->delivery_date}}" disabled style="padding-top: 0px">
	                            @error('delivery_date')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

                            <div class="form-group @error('delivery_time') has-error @enderror">
	                            <label for="delivery_time">Delivery time</label>
	                            <input id="delivery_time" type="time" class="form-control" name="delivery_time" value="{{$delivery->delivery_time}}" disabled style="padding-top: 0px">
	                            @error('delivery_time')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('pickup_address') has-error @enderror">
	                            <label for="pickup_address">Pickup address</label>
	                            <input id="pickup_address" type="text" name="pickup_address" class="form-control" value="{{$address[2]['data']}}" disabled>
	                            @error('pickup_address')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        <div id="first-address-field"> <b>Delivery details</b>
		                        <div class="form-group @error('order_no') has-error @enderror">
		                            <label for="order_no">Order No</label>
		                            <input id="order_no" type="text" class="form-control" name="order_no" value="{{$delivery->order_no}}" disabled>
		                            @error('order_no')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('name') has-error @enderror">
		                            <label for="name">Name</label>
		                            <input id="name" type="text" class="form-control" name="name" value="{{$delivery->name}}" disabled>
		                            @error('name')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('postal_code') has-error @enderror">
		                            <label for="postal_code">Postal Code</label>
		                            <input id="postal_code" type="number" class="form-control" name="postal_code" value="{{$delivery->postal_code}}" disabled>
		                            @error('postal_code')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                            <span id="postal_code_error_message" class="help-block"></span>
		                        </div>

		                        <div class="form-group @error('address') has-error @enderror">
		                            <label for="address">Address</label>
		                            <input id="address" type="text" name="address" class="form-control" value="{{$delivery->delivery_address}}" disabled>
		                            @error('address')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('unit_no') has-error @enderror">
		                            <label for="unit_no">Unit No</label>
		                            <input id="unit_no" type="text" class="form-control" name="unit_no" value="{{$delivery->unit_no}}" disabled>
		                            @error('unit_no')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('contact') has-error @enderror">
		                            <label for="contact">Contact</label>
		                            <input id="contact" type="tel" class="form-control" name="contact" value="{{$delivery->contact}}" disabled>
		                            @error('contact')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>
		                    </div>

	                        <div id="delivery_fee" class="form-group @error('fee') has-error @enderror">
	                            <label for="fee">Fee (SGD)</label>
	                            <input id="fee" type="tel" class="form-control" value="{{$delivery->fee}}" disabled>
	                            @error('fee')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        @if(!old('has_add_one') && $delivery->option_one == 0)
		                        <div id="first-additional-button" class="form-group">
		                        	<button type="button" onclick="addAdditionalOne()" name="add-additional-fee-one" class="btn-primary">Add Additional Fee</button>
		                        </div>
		                    @else
		                    	<div id="first-additional-field"><b>1st Additional Fee</b>
	                        		<input type="hidden" name="has_add_one" value="true">

		                        	<div id="remove-first-field" class="form-group" @if(old("has_add_two") || $delivery->option_two != 0) style="display: none" @endif>
		                        		<button type="button" onclick="removeAdditionalOne()" name="remove-first-add-field" class="btn-danger">Remove first additional fee</button>
		                        	</div>

		                        	<div class="form-group @error('option_one') has-error @enderror">
			                            <label for="option_one">Additional Fee's Option</label>
			                            <select id="option_one" name="option_one" class="form-control">
			                            	@if(old("option_one")) 
			                            		@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if(old("option_one") == $option->id) selected @endif>{{ $option->name }}</option>
				                            	@endforeach
			                            	@else
				                            	@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if($delivery->get_option_one == $option->id) selected @endif>{{ $option->name }}</option>
				                            	@endforeach
			                            	@endif
			                            </select>
			                            @error('option_one')
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>

		                        	<div id="fee_one" class="form-group @error('value_one') has-error @enderror">
			                            <label for="value_one">Additional Fee (SGD)</label>
			                            <input id="value_one" type="number" min="0" step="0.01" class="form-control" name="value_one" @if(old("value_one")) value="{{ old("value_one")}}" @elseif($delivery->value_one != 0) value="{{$delivery->value_one}}" @else value="0" @endif onkeyup="firstValue()" required>
			                            @error("value_one")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>

			                        @if (!old("has_add_two")  && $delivery->option_two == 0)
				                    <div id="second-additional-button" class="form-group">
			                        	<button type="button" onclick="addAdditionalTwo()" name="add-additional-fee-two" class="btn-primary">Add 2nd Additional Fee</button>
			                        </div>
			                        @endif
			                    </div>
		                    @endif

		                    @if(old("has_add_two") || $delivery->option_two != 0)
	                        	<div id="second-additional-field"><b>2nd Additional Fee</b>
	                        		<input type="hidden" name="has_add_two" value="true">

		                        	<div id="remove-second-field" class="form-group" @if(old("has_add_three") || $delivery->option_three != 0) style="display: none" @endif>
		                        		<button type="button" onclick="removeAdditionalTwo()" name="remove-second-add-field" class="btn-danger">Remove second additional fee</button>
		                        	</div>

		                        	<div class="form-group @error("option_two") has-error @enderror">
			                            <label for="option_two">Additional Fee"s Option</label>
			                            <select id="option_two" name="option_two" class="form-control">
			                            	@if(old("option_two")) 
			                            		@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if(old("option_two") == $option->id) selected @endif>{{ $option->name }}</option>
				                            	@endforeach
			                            	@else
				                            	@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if($delivery->get_option_two == $option->id) selected @endif >{{ $option->name }}</option>
				                            	@endforeach
				                            @endif
			                            </select>
			                            @error("option_two")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>

		                        	<div id="fee_two" class="form-group @error("value_two") has-error @enderror">
			                            <label for="value_two">Additional Fee (SGD)</label>
			                            <input id="value_two" type="number" min="0" step="0.01" class="form-control" name="value_two" @if(old("value_two")) value="{{old("value_two")}}" @elseif($delivery->value_two != 0) value="{{$delivery->value_two}}" @else value="0" @endif onkeyup="secondValue()" required>
			                            @error("value_two")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>
			                        
			                        @if (!old("has_add_three")  && $delivery->option_three == 0)
					                    <div id="third-additional-button" class="form-group">
				                        	<button type="button" onclick="addAdditionalThree()" name="add-additional-fee-two" class="btn-primary">Add 3rd Additional Fee</button>
				                        </div>
			                        @endif	
			                    </div>	                        
	                        @endif

	                        @if(old("has_add_three") || $delivery->option_three != 0)
		                        <div id="third-additional-field"><b>3rd Additional Fee</b>
	                        		<input type="hidden" name="has_add_three" value="true">

		                        	<div id="remove-third-field" class="form-group" @if(old("has_add_four") || $delivery->option_four != 0) style="display: none" @endif>
		                        		<button type="button" onclick="removeAdditionalThree()" name="remove-third-add-field" class="btn-danger">Remove third additional fee</button>
		                        	</div>

		                        	<div class="form-group @error("option_three") has-error @enderror">
			                            <label for="option_three">Additional Fee"s Option</label>
			                            <select id="option_three" name="option_three" class="form-control">
			                            	@if(old("option_three")) 
			                            		@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if(old("option_three") == $option->id) selected @endif>{{ $option->name }}</option>
				                            	@endforeach
			                            	@else
				                            	@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if($delivery->get_option_three == $option->id) selected @endif >{{ $option->name }}</option>
				                            	@endforeach
				                            @endif
			                            </select>
			                            @error("option_three")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>

		                        	<div id="fee_three" class="form-group @error("value_three") has-error @enderror">
			                            <label for="value_three">Additional Fee (SGD)</label>
			                            <input id="value_three" type="number" min="0" step="0.01" class="form-control" name="value_three" @if(old("value_three")) value="{{old("value_three")}}" @elseif($delivery->value_three != 0) value="{{$delivery->value_three}}" @else value="0" @endif onkeyup="thirdValue()" required>
			                            @error("value_three")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>
				                        
				                    @if (!old("has_add_four") && $delivery->option_four == 0)
					                    <div id="fourth-additional-button" class="form-group">
				                        	<button type="button" onclick="addAdditionalFour()" name="add-additional-fee-four" class="btn-primary">Add 4th Additional Fee</button>
				                        </div>
				                    @endif	
				                </div>                    
	                        @endif

	                        @if(old("has_add_four") || $delivery->option_four != 0)
	                        	<div id="fourth-additional-field"><b>4th Additional Fee</b>
	                        		<input type="hidden" name="has_add_four" value="true">

		                        	<div id="remove-fourth-field" class="form-group">
		                        		<button type="button" onclick="removeAdditionalFour()" name="remove-fourth-add-field" class="btn-danger">Remove fourth additional fee</button>
		                        	</div>

		                        	<div class="form-group @error("option_four") has-error @enderror">
			                            <label for="option_four">Additional Fee"s Option</label>
			                            <select id="option_four" name="option_four" class="form-control">
			                            	@if(old("option_four")) 
			                            		@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if(old("option_four") == $option->id) selected @endif>{{ $option->name }}</option>
				                            	@endforeach
			                            	@else
				                            	@foreach($options as $option)
				                            		<option value="{{ $option->id }}" @if($delivery->get_option_four == $option->id) selected @endif >{{ $option->name }}</option>
				                            	@endforeach
				                            @endif
			                            </select>
			                            @error("option_four")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>

		                        	<div class="form-group @error("value_four") has-error @enderror">
			                            <label for="value_four">Additional Fee (SGD)</label>
			                            <input id="value_four" type="number" min="0" step="0.01" class="form-control" name="value_four" @if(old("value_four")) value="{{old("value_four")}}" @elseif($delivery->value_four != 0) value="{{$delivery->value_four}}" @else value="0" @endif onkeyup="fourthValue()" required>
			                            @error("value_four")
			                                <span class="help-block">{{ $message }}</span>
			                            @enderror
			                        </div>
			                    </div>	                        
	                        @endif

	                        <div id="total-field" class="form-group @error('total_fee') has-error @enderror">
	                            <label for="total_fee">Total Fee (SGD)</label>
	                            <input id="total_fee" type="tel" class="form-control" name="total_fee" value="{{$delivery->total_fee}}" disabled>
	                            @error('total_fee')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('status') has-error @enderror">
	                            <label for="status">Status</label>
	                            <select id="status" name="status" class="form-control">
	                            	@foreach($deliveryStatus as $key => $status)
	                            		@if($status == 'In Progress' || $status == 'Successful' || $status == 'Failed')
	                            		<option value="{{ $key }}" @if($delivery->status == $key) selected @endif>{{ $status }}</option>
	                            		@elseif($status == 'Unassigned')
	                            		<option value="{{ $key }}" @if($delivery->status == $key) selected @endif>Pending</option>
	                            		@elseif($status == 'Cancel')
	                            		<option value="{{ $key }}" @if($delivery->status == $key) selected @endif>Cancelled</option>
	                            		@endif
	                            	@endforeach
	                            </select>
	                            @error('status')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <input type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-delivery" type="submit" onclick="disableButton()" class="btn btn-primary">Save</button>
	                            <a href="{{ route('delivery.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection
@push('additional_js')
    <script>
    	$( document ).ready(function() {
		    calculateFee();
		});

    	function disableButton() {
	        var is_empty = false;
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('disabled')){
	                is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }

	    var deliveryFee = '{{$delivery->fee}}';

	    if(typeof($('#value_one').val()) === 'undefined') {
	    	var addOne = 0;
	    }
	    else {
	    	var addOne = $('#value_one').val();
	    }
	    if(typeof($('#value_two').val()) === 'undefined') {
	    	var addTwo = 0;
	    }
	    else {
	    	var addTwo = $('#value_two').val();
	    }
	    if(typeof($('#value_three').val()) === 'undefined') {
	    	var addThree = 0;
	    }
	    else {
	    	var addThree = $('#value_three').val();
	    }
	    if(typeof($('#value_four').val()) === 'undefined') {
	    	var addFour = 0;
	    }
	    else {
	    	var addFour = $('#value_four').val();
	    }

	    function addAdditionalOne() {
        	const domElement = $('<div id="first-additional-field"><b>1st Additional Fee</b><input type="hidden" name="has_add_one" value="true"><div id="remove-first-field" class="form-group"><button type="button" onclick="removeAdditionalOne()" name="remove-first-add-field" class="btn-danger">Remove first additional fee</button></div><div class="form-group @error("option_one") has-error @enderror"><label for="option_one">Additional Fee\'s Option</label><select id="option_one" name="option_one" class="form-control">@if(old("option_one"))@foreach($options as $option) <option value="{{ $option->id }}" @if(old("option_one") == $option->id) selected @endif>{{ $option->name }}</option> @endforeach @else @foreach($options as $option)<option value="{{ $option->id }}" @if($delivery->get_option_one == $option->id) selected @endif>{{ $option->name }}</option>@endforeach @endif</select>@error("option_one")<span class="help-block">{{ $message }}</span>@enderror</div><div id="fee_one" class="form-group @error("value_one") has-error @enderror"><label for="value_one">Additional Fee (SGD)</label><input id="value_one" type="number" min="0" step="0.01" class="form-control" name="value_one" @if(old("value_one")) value="{{ old("value_one")}}" @elseif($delivery->value_one != 0) value="{{$delivery->value_one}}" @else value="0" @endif onkeyup="firstValue()" required>@error("value_one")<span class="help-block">{{ $message }}</span>@enderror</div><div id="second-additional-button" class="form-group"><button type="button" onclick="addAdditionalTwo()" name="add-additional-fee-two" class="btn-primary">Add 2nd Additional Fee</button></div>');
    		$("#first-additional-button").after(domElement);
    		$("#first-additional-button").remove();
    		addOne = $('#value_one').val();
    		calculateFee();
        }

        function addAdditionalTwo() {
        	$('#remove-first-field').prop('disabled', true);
	    	$('#remove-first-field').hide();
        	const domElement = $('<div id="second-additional-field"><b>2nd Additional Fee</b><input type="hidden" name="has_add_two" value="true"><div id="remove-second-field" class="form-group"><button type="button" onclick="removeAdditionalTwo()" name="remove-second-add-field" class="btn-danger">Remove second additional fee</button></div><div class="form-group @error("option_two") has-error @enderror"><label for="option_two">Additional Fee\'s Option</label><select id="option_two" name="option_two" class="form-control">@if(old("option_two")) @foreach($options as $option) <option value="{{ $option->id }}" @if(old("option_two") == $option->id) selected @endif>{{ $option->name }}</option> @endforeach @else @foreach($options as $option) <option value="{{ $option->id }}" @if($delivery->get_option_two == $option->id) selected @endif >{{ $option->name }}</option> @endforeach @endif</select>@error("option_two")<span class="help-block">{{ $message }}</span>@enderror</div><div id="fee_two" class="form-group @error("value_two") has-error @enderror"><label for="value_two">Additional Fee (SGD)</label><input id="value_two" type="number" min="0" step="0.01" class="form-control" name="value_two" @if(old("value_two")) value="{{old("value_two")}}" @elseif($delivery->value_two != 0) value="{{$delivery->value_two}}" @else value="0" @endif onkeyup="secondValue()" required>@error("value_two")<span class="help-block">{{ $message }}</span>@enderror</div><div id="third-additional-button" class="form-group"><button type="button" onclick="addAdditionalThree()" name="add-additional-fee-three" class="btn-primary">Add 3rd Additional Fee</button></div>');
    		$("#first-additional-field").after(domElement);
    		$("#second-additional-button").remove();
    		addTwo = $('#value_two').val();
    		calculateFee();
        }

        function addAdditionalThree() {
        	$('#remove-second-field').prop('disabled', true);
	    	$('#remove-second-field').hide();
        	const domElement = $('<div id="third-additional-field"><b>3rd Additional Fee</b><input type="hidden" name="has_add_three" value="true"><div id="remove-third-field" class="form-group"><button type="button" onclick="removeAdditionalThree()" name="remove-third-add-field" class="btn-danger">Remove third additional fee</button></div><div class="form-group @error("option_three") has-error @enderror"><label for="option_three">Additional Fee\'s Option</label><select id="option_three" name="option_three" class="form-control">@if(old("option_three")) @foreach($options as $option) <option value="{{ $option->id }}" @if(old("option_three") == $option->id) selected @endif>{{ $option->name }}</option> @endforeach @else @foreach($options as $option) <option value="{{ $option->id }}" @if($delivery->get_option_three == $option->id) selected @endif >{{ $option->name }}</option> @endforeach @endif</select>@error("option_three")<span class="help-block">{{ $message }}</span>@enderror</div><div id="fee_three" class="form-group @error("value_three") has-error @enderror"><label for="value_three">Additional Fee (SGD)</label><input id="value_three" type="number" min="0" step="0.01" class="form-control" name="value_three" @if(old("value_three")) value="{{old("value_three")}}" @elseif($delivery->value_three != 0) value="{{$delivery->value_three}}" @else value="0" @endif onkeyup="thirdValue()" required>@error("value_three")<span class="help-block">{{ $message }}</span>@enderror</div><div id="fourth-additional-button" class="form-group"><button type="button" onclick="addAdditionalFour()" name="add-additional-fee-four" class="btn-primary">Add 4th Additional Fee</button></div>');
    		$("#second-additional-field").after(domElement);
    		$("#third-additional-button").remove();
    		addThree = $('#value_three').val();
    		calculateFee();
        }

        function addAdditionalFour() {
        	$('#remove-third-field').prop('disabled', true);
	    	$('#remove-third-field').hide();
        	const domElement = $('<div id="fourth-additional-field"><b>4th Additional Fee</b><input type="hidden" name="has_add_four" value="true"><div id="remove-fourth-field" class="form-group"><button type="button" onclick="removeAdditionalFour()" name="remove-fourth-add-field" class="btn-danger">Remove fourth additional fee</button></div><div class="form-group @error("option_four") has-error @enderror"><label for="option_four">Additional Fee\'s Option</label><select id="option_four" name="option_four" class="form-control">@if(old("option_four")) @foreach($options as $option) <option value="{{ $option->id }}" @if(old("option_four") == $option->id) selected @endif>{{ $option->name }}</option> @endforeach @else @foreach($options as $option) <option value="{{ $option->id }}" @if($delivery->get_option_four == $option->id) selected @endif >{{ $option->name }}</option> @endforeach @endif</select>@error("option_four")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("value_four") has-error @enderror"><label for="value_four">Additional Fee (SGD)</label><input id="value_four" type="number" min="0" step="0.01" class="form-control" name="value_four" @if(old("value_four")) value="{{old("value_four")}}" @elseif($delivery->value_four != 0) value="{{$delivery->value_four}}" @else value="0" @endif onkeyup="fourthValue()" required>@error("value_four")<span class="help-block">{{ $message }}</span>@enderror</div>');
    		$("#third-additional-field").after(domElement);
    		$("#fourth-additional-button").remove();
    		addFour = $('#value_four').val();
    		calculateFee();
        }

        function calculateFee() {
			feeOne = parseFloat(addOne);
			feeTwo = parseFloat(addTwo);
			feeThree = parseFloat(addThree);
			feeFour = parseFloat(addFour);
			delivery = parseFloat(deliveryFee);
			console.log(addOne);
			console.log(addTwo);
			console.log(addThree);
			console.log(addFour);
			const total_fee = delivery + feeOne + feeTwo + feeThree + feeFour;
			$('#total_fee').val(total_fee.toFixed(2));
		}

		function removeAdditionalOne() {
			const domElement = $('<div id="first-additional-button" class="form-group"><button type="button" onclick="addAdditionalOne()" name="add-additional-fee-one" class="btn-primary">Add Additional Fee</button></div>');
			$("#delivery_fee").after(domElement);
			$("div#first-additional-field").remove();
			addOne = 0;
			calculateFee();
		}

		function removeAdditionalTwo() {
			$('#remove-first-field').prop('disabled', false);
	    	$('#remove-first-field').show();
			const domElement = $('<div id="second-additional-button" class="form-group"><button type="button" onclick="addAdditionalTwo()" name="add-additional-fee-two" class="btn-primary">Add 2nd Additional Fee</button></div>');
			$("#fee_one").after(domElement);
			$("div#second-additional-field").remove();
			addTwo = 0;
			calculateFee();
		}

		function removeAdditionalThree() {
			$('#remove-second-field').prop('disabled', false);
	    	$('#remove-second-field').show();
			const domElement = $('<div id="third-additional-button" class="form-group"><button type="button" onclick="addAdditionalThree()" name="add-additional-fee-three" class="btn-primary">Add 3rd Additional Fee</button></div>');
			$("#fee_two").after(domElement);
			$("div#third-additional-field").remove();
			addThree = 0;
			calculateFee();
		}

		function removeAdditionalFour() {
			$('#remove-third-field').prop('disabled', false);
	    	$('#remove-third-field').show();
			const domElement = $('<div id="fourth-additional-button" class="form-group"><button type="button" onclick="addAdditionalFour()" name="add-additional-fee-four" class="btn-primary">Add 4th Additional Fee</button></div>');
			$("#fee_three").after(domElement);
			$("div#fourth-additional-field").remove();
			addFour = 0;
			calculateFee();
		}

		function firstValue() {
			addOne = $('#value_one').val();
			if (addOne.charAt(0) == 0 && addOne.length >= 2) {
				if (addOne.charAt(1) != '.') {
					$('#value_one').val(addOne.replace(/^0+/, ''));
				}
				else {
					const str = addOne.split('.');
					if (str[1].length > 2) {
						$('#value_one').val(parseFloat(addOne).toFixed(2));
					}
				}
			}
			else if(addOne.charAt(0) != 0) {
				if (addOne.indexOf('.') == -1) {
					// do nothing
				}
				else {
					const str = addOne.split('.');
					if (str[1].length > 2) {
						$('#value_one').val(parseFloat(addOne).toFixed(2));
					}
				}
			}
			calculateFee();
		}

		function secondValue() {
			addTwo = $('#value_two').val();
			if (addTwo.charAt(0) == 0 && addTwo.length >= 2) {
				if (addTwo.charAt(1) != '.') {
					$('#value_two').val(addTwo.replace(/^0+/, ''));
				}
				else {
					const str = addTwo.split('.');
					if (str[1].length > 2) {
						$('#value_two').val(parseFloat(addTwo).toFixed(2));
					}
				}
			}
			else if(addTwo.charAt(0) != 0) {
				if (addTwo.indexOf('.') == -1) {
					// do nothing
				}
				else {
					const str = addTwo.split('.');
					if (str[1].length > 2) {
						$('#value_two').val(parseFloat(addTwo).toFixed(2));
					}
				}
			}
			calculateFee();
		}

		function thirdValue() {
			addThree = $('#value_three').val();
			if (addThree.charAt(0) == 0 && addThree.length >= 2) {
				if (addThree.charAt(1) != '.') {
					$('#value_three').val(addThree.replace(/^0+/, ''));
				}
				else {
					const str = addThree.split('.');
					if (str[1].length > 2) {
						$('#value_three').val(parseFloat(addThree).toFixed(2));
					}
				}
			}
			else if(addThree.charAt(0) != 0) {
				if (addThree.indexOf('.') == -1) {
					// do nothing
				}
				else {
					const str = addThree.split('.');
					if (str[1].length > 2) {
						$('#value_three').val(parseFloat(addThree).toFixed(2));
					}
				}
			}
			calculateFee();
		}

		function fourthValue() {
			addFour = $('#value_four').val();
			if (addFour.charAt(0) == 0 && addFour.length >= 2) {
				if (addFour.charAt(1) != '.') {
					$('#value_four').val(addFour.replace(/^0+/, ''));
				}
				else {
					const str = addFour.split('.');
					if (str[1].length > 2) {
						$('#value_four').val(parseFloat(addFour).toFixed(2));
					}
				}
			}
			else if(addFour.charAt(0) != 0) {
				if (addFour.indexOf('.') == -1) {
					// do nothing
				}
				else {
					const str = addFour.split('.');
					if (str[1].length > 2) {
						$('#value_four').val(parseFloat(addFour).toFixed(2));
					}
				}
			}
			calculateFee();
		}

	</script>
@endpush