@extends('adminlte::page')

@section('title', 'Delivery Form')

@section('content_header')
    <h1>Add new delivery</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('delivery.store') }}" enctype="multipart/form-data">
	                        @method('POST') 
	                        @csrf

	                        <div class="form-group @error('delivery_date') has-error @enderror">
	                            <label for="delivery_date">Delivery date</label>
	                            <input id="delivery_date" type="date" class="form-control" name="delivery_date" value="{{ old('delivery_date') }}" required style="padding-top: 0px">
	                            @error('delivery_date')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

                            <div class="form-group @error('delivery_time') has-error @enderror">
	                            <label for="delivery_time">Delivery time</label>
	                            <input id="delivery_time" type="time" class="form-control" name="delivery_time" value="{{ old('delivery_time') }}" required style="padding-top: 0px">
	                            @error('delivery_time')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('pickup_address') has-error @enderror">
	                            <label for="pickup_address">Pickup address</label>
	                            <select id="pickup_address" name="pickup_address" class="form-control">
	                            	<option value="new-address">Add new address</option>
	                            	@foreach($addresses as $address)
	                            		@if(old('pickup_address') === NULL)
	                            		<option value="{{$address->id}}" @if($loop->first) selected @endif>{{ $address->name }}, {{$address->address}} #{{$address->unit_no}} S{{$address->postal_code}}</option>
	                            		@else
	                            		<option value="{{$address->id}}" @if(old('pickup_address') == $address->id) selected @endif>{{ $address->name }}, {{$address->address}} #{{$address->unit_no}} S{{$address->postal_code}}</option>
	                            		@endif
	                            	@endforeach
	                            </select>
	                            @error('pickup_address')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>
	                        <div id="first-address-field"> <b>Delivery details</b>
		                        <div class="form-group @error('order_no_one') has-error @enderror">
		                            <label for="order_no_one">Order No</label>
		                            <input id="order_no_one" type="text" class="form-control" name="order_no_one" value="{{ old('order_no_one') }}" required>
		                            @error('order_no_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('name_one') has-error @enderror">
		                            <label for="name_one">Name</label>
		                            <input id="name_one" type="text" class="form-control" name="name_one" value="{{ old('name_one') }}" required>
		                            @error('name_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('postal_code_one') has-error @enderror">
		                            <label for="postal_code_one">Postal Code</label>
		                            <input id="postal_code_one" type="number" class="form-control" onkeyup="disableSubmit(1)" name="postal_code_one" value="{{ old('postal_code_one') }}" required>
		                            @error('postal_code_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                            <span id="postal_code_one_error_message" class="help-block"></span>
		                            <button type="button" onclick="getAddressOne()" class="btn-primary">Get address</button>
		                            <span class="get-address-error-one"></span>
		                        </div>

		                        <div class="form-group @error('address_one') has-error @enderror">
		                            <label for="get_address_one">Address</label>
		                            <input id="get_address_one" type="text" class="form-control" value="{{ old('address_one') }}" required disabled>
		                            <input id="address_one" type="hidden" name="address_one" class="form-control" value="{{ old('address_one') }}" required>
		                            <input id="longitude_one" type="hidden" name="longitude_one" class="form-control" value="{{ old('longitude_one') }}" required>
			                        <input id="latitude_one" type="hidden" name="latitude_one" class="form-control" value="{{ old('latitude_one') }}" required>
		                            @error('address_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('unit_no_one') has-error @enderror">
		                            <label for="unit_no_one">Unit No (Key ‘0’ if no unit no)</label>
		                            <input id="unit_no_one" type="text" class="form-control" name="unit_no_one" value="{{ old('unit_no_one') }}" required>
		                            @error('unit_no_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div id="phone_one_field" class="form-group @error('phone_one') has-error @enderror">
		                            <label for="phone_one">Contact</label>
		                            <input id="phone_one" type="tel" class="form-control" name="phone_one" value="{{ old('phone_one') }}" required>
		                            @error('phone_one')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>
		                        @if(!old('has_address_two'))
		                        <div class="form-group">
		                        	<button type="button" onclick="addAddressTwo()" name="add-second-delivery-address" class="btn-primary">Add another delivery address</button>
		                        </div>
		                        @endif
		                    </div>

	                        @if(old('has_address_two'))
	                        <div id="second-address-field"><b>2nd Delivery details</b>
	                        	<input type="hidden" name="has_address_two" value="true">

	                        	<div id="remove-address-two-field" class="form-group">
	                        		<button type="button" onclick="removeAddressTwo()" name="remove-second-delivery-address" class="btn-danger">Remove 2nd delivery address</button>
	                        	</div>

	                        	<div class="form-group @error('order_no_two') has-error @enderror">
		                            <label for="order_no_two">Order No</label>
		                            <input id="order_no_two" type="text" class="form-control" name="order_no_two" value="{{ old('order_no_two') }}" required>
		                            @error('order_no_two')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

	                        	<div class="form-group @error('name_two') has-error @enderror">
		                            <label for="name_two">Name</label>
		                            <input id="name_two" type="text" class="form-control" name="name_two" value="{{ old('name_two') }}" required>
		                            @error('name_two')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('postal_code_two') has-error @enderror">
		                        	<label for="postal_code_two">Postal Code</label>
		                        	<input id="postal_code_two" type="number" class="form-control" onkeyup="disableSubmit(2)" name="postal_code_two" value="{{ old('postal_code_two') }}" required>
		                        	@error("postal_code_two")
		                        		<span class="help-block">{{ $message }}</span>
		                        	@enderror
		                        	<span id="postal_code_two_error_message" class="help-block"></span>
		                        	<button type="button" onclick="getAddressTwo()" class="btn-primary">Get address</button>
		                        	<span class="get-address-error-two"></span>
		                        </div>

		                        <div class="form-group @error('address_two') has-error @enderror">
		                        	<label for="get_address_two">Address</label>
		                        	<input id="get_address_two" type="text" class="form-control" value="{{ old('address_two') }}" required disabled>
		                        	<input id="address_two" type="hidden" name="address_two" class="form-control" value="{{ old('address_two') }}" required>
		                        	<input id="longitude_two" type="hidden" name="longitude_two" class="form-control" value="{{ old('longitude_two') }}" required>
			                        <input id="latitude_two" type="hidden" name="latitude_two" class="form-control" value="{{ old('latitude_two') }}" required>
		                        	@error("address_two")
		                        		<span class="help-block">{{ $message }}</span>
		                        	@enderror
		                        </div>

		                        <div class="form-group @error('unit_no_two') has-error @enderror">
		                            <label for="unit_no_two">Unit No (Key ‘0’ if no unit no)</label>
		                            <input id="unit_no_two" type="text" class="form-control" name="unit_no_two" value="{{ old('unit_no_two') }}" required>
		                            @error('unit_no_two')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

	                        	<div id="phone_two_field" class="form-group @error('phone_two') has-error @enderror">
		                            <label for="phone_two">Contact</label>
		                            <input id="phone_two" type="tel" class="form-control" name="phone_two" value="{{ old('phone_two') }}" required>
		                            @error('phone_two')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>
		                        @if (!old('has_address_three'))
			                    <div class="form-group">
			                        	<button type="button" onclick="addAddressThree()" name="add-third-delivery-address" class="btn-primary">Add another delivery address</button>
			                        </div>
			                    </div>
			                    @endif		                        
	                        @endif

	                        @if(old('has_address_three'))
	                        <div id="third-address-field"><b>3rd Delivery details</b>
	                        	<input type="hidden" name="has_address_three" value="true">

	                        	<div class="form-group">
	                        		<button type="button" onclick="removeAddressThree()" name="remove-third-delivery-address" class="btn-danger">Remove 3rd delivery address</button>
	                        	</div>

	                        	<div class="form-group @error('order_no_three') has-error @enderror">
		                            <label for="order_no_three">Order No</label>
		                            <input id="order_no_three" type="text" class="form-control" name="order_no_three" value="{{ old('order_no_three') }}" required>
		                            @error('order_no_three')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

	                        	<div class="form-group @error('name_three') has-error @enderror">
		                            <label for="name_three">Name</label>
		                            <input id="name_three" type="text" class="form-control" name="name_three" value="{{ old('name_three') }}" required>
		                            @error('name_three')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>

		                        <div class="form-group @error('postal_code_three') has-error @enderror">
		                        	<label for="postal_code_three">Postal Code</label>
		                        	<input id="postal_code_three" type="number" class="form-control" onkeyup="disableSubmit(3)" name="postal_code_three" value="{{ old('postal_code_three') }}" required>
		                        	@error("postal_code_three")
		                        		<span class="help-block">{{ $message }}</span>
		                        	@enderror
		                        	<span id="postal_code_three_error_message" class="help-block"></span>
		                        	<button type="button" onclick="getAddressThree()" class="btn-primary">Get address</button>
		                        	<span class="get-address-error-three"></span>
		                        </div>

		                        <div class="form-group @error('address_three') has-error @enderror">
		                        	<label for="get_address_three">Address</label>
			                        <input id="get_address_three" type="text" class="form-control" value="{{ old('address_three') }}" required disabled>
			                        <input id="address_three" type="hidden" name="address_three" class="form-control" value="{{ old('address_three') }}" required>
			                        <input id="longitude_three" type="hidden" name="longitude_three" class="form-control" value="{{ old('longitude_three') }}" required>
			                        <input id="latitude_three" type="hidden" name="latitude_three" class="form-control" value="{{ old('latitude_three') }}" required>
			                        @error("address_three")
				                        <span class="help-block">{{ $message }}</span>
				                    @enderror
				                </div>

				                <div class="form-group @error('unit_no_three') has-error @enderror">
		                            <label for="unit_no_three">Unit No (Key ‘0’ if no unit no)</label>
		                            <input id="unit_no_three" type="text" class="form-control" name="unit_no_three" value="{{ old('unit_no_three') }}" required>
		                            @error('unit_no_three')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
	                        	</div>

	                        	<div class="form-group @error('phone_three') has-error @enderror">
		                            <label for="phone_three">Contact</label>
		                            <input id="phone_three" type="tel" class="form-control" name="phone_three" value="{{ old('phone_three') }}" required>
		                            @error('phone_three')
		                                <span class="help-block">{{ $message }}</span>
		                            @enderror
		                        </div>
		                    </div>
			                @endif

	                        <div class="form-group @error('fee') has-error @enderror">
	                            <label for="get_fee">Fee (SGD)</label>
	                            <input id="get_fee" type="tel" class="form-control" value="{{ old('fee', 0) }}" required disabled>
	                            <input id="fee" type="hidden" name="fee" class="form-control" value="{{ old('fee', 0) }}" required>
	                            @error('fee')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <input type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-delivery" type="submit" onclick="disableButton()" class="btn btn-primary">Save</button>
	                            <a href="{{ route('delivery.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection
@push('additional_js')
    <script>
    	var triggered_one = true;
    	var triggered_two = true;
    	var triggered_three = true;

    	function disableSubmit(value) {
    		$('#submit-delivery').prop('disabled', true);
    		if (value == 1) {
    			triggered_one = false;
    			$('span.get-address-error-one').text('Remember to click on the "Get address" button to update the address field and recalculate the fee.');
    		}
    		else if (value == 2) {
    			triggered_two = false;
    			$('span.get-address-error-two').text('Remember to click on the "Get address" button to update the address field and recalculate the fee.');
    		}
    		else if (value == 3) {
    			triggered_three = false;
    			$('span.get-address-error-three').text('Remember to click on the "Get address" button to update the address field and recalculate the fee.');
    		}
    		else {
    			// do nothing
    		}
    		
    	}

    	function disableButton() {
	        var is_empty = false;
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }

    	var addressOne = 0;
    	var addressTwo = 0;
    	var addressThree = 0;

    	$('select').change(function(){
    		if(document.getElementById('pickup_address').value == "new-address") {
    			window.location.href = "{{route('address.create')}}";
    		}
    		else {
    			if ($('#get_address_one').val() == $('#address_one').val() && $('#get_address_one').val() != '') {
            		getAddressOne();
            	}
            	if ($('#has_address_two').length) {
            		if ($('#get_address_two').val() == $('#address_two').val() && $('#get_address_two').val() != '') {
            			getAddressTwo();
            		}
            	}
            	if ($('#has_address_three').length) {
            		if ($('#get_address_three').val() == $('#address_three').val() && $('#get_address_three').val() != '') {
            			getAddressThree();
            		}
            	}
    		}
    	})

	    function addAddressTwo() {
        	const domElement = $('<div id="second-address-field"><b>2nd Delivery details</b><input type="hidden" name="has_address_two" value="true"><div  id="remove-address-two-field" class="form-group"><button type="button" onclick="removeAddressTwo()" name="remove-second-delivery-address" class="btn-danger">Remove 2nd delivery address</button></div><div class="form-group @error("order_no_two") has-error @enderror"><label for="order_no_two">Order No</label><input id="order_no_two" type="text" class="form-control" name="order_no_two" value="{{ old("order_no_two") }}" required>@error("order_no_two")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("name_two") has-error @enderror"><label for="name_two">Name</label><input id="name_two" type="text" class="form-control" name="name_two" value="{{ old("name_two") }}" required>@error("name_two")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("postal_code_two") has-error @enderror"><label for="postal_code_two">Postal Code</label><input id="postal_code_two" type="number" class="form-control" onkeyup="disableSubmit(2)" name="postal_code_two" value="{{ old("postal_code_two") }}" required>@error("postal_code_two")<span class="help-block">{{ $message }}</span>@enderror<span id="postal_code_two_error_message" class="help-block"></span><button type="button" onclick="getAddressTwo()" class="btn-primary">Get address</button><span class="get-address-error-two"></span></div><div class="form-group @error("address") has-error @enderror"><label for="get_address_two">Address</label><input id="get_address_two" type="text" class="form-control" value="{{ old("address_two") }}" required disabled><input id="address_two" type="hidden" name="address_two" class="form-control" value="{{ old("address_two") }}" required><input id="longitude_two" type="hidden" name="longitude_two" class="form-control" value="{{ old("longitude_two") }}" required><input id="latitude_two" type="hidden" name="latitude_two" class="form-control" value="{{ old("latitude_two") }}" required>@error("address_two")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("unit_no_two") has-error @enderror"><label for="unit_no_two">Unit No (Key ‘0’ if no unit no)</label><input id="unit_no_two" type="text" class="form-control" name="unit_no_two" value="{{ old("unit_no_two") }}" required>@error("unit_no_two")<span class="help-block">{{ $message }}</span>@enderror</div><div id="phone_two_field" class="form-group @error("phone_two") has-error @enderror"><label for="phone_two">Contact</label><input id="phone_two" type="tel" class="form-control" name="phone_two" value="{{ old("phone_two") }}" required>@error("phone_two")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group"><button type="button" onclick="addAddressThree()" name="add-third-delivery-address" class="btn-primary">Add another delivery address</button></div></div>');
    		$("div#first-address-field").after(domElement);
    		$("button[name='add-second-delivery-address']").remove();
        }
                    

	    function addAddressThree() {
	    	$('#remove-address-two-field').prop('disabled', true);
	    	$('#remove-address-two-field').hide();
        	const domElement = $('<div id="third-address-field"><b>3rd Delivery details</b><input type="hidden" name="has_address_three" value="true"><div class="form-group"><button type="button" onclick="removeAddressThree()" name="remove-third-delivery-address" class="btn-danger">Remove 3rd delivery address</button></div><div class="form-group @error("order_no_three") has-error @enderror"><label for="order_no_three">Order No</label><input id="order_no_three" type="text" class="form-control" name="order_no_three" value="{{ old("order_no_three") }}" required>@error("order_no_three")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("name_three") has-error @enderror"><label for="name_three">Name</label><input id="name_three" type="text" class="form-control" name="name_three" value="{{ old("name_three") }}" required>@error("name_three")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("postal_code_three") has-error @enderror"><label for="postal_code_three">Postal Code</label><input id="postal_code_three" type="number" class="form-control" onkeyup="disableSubmit(3)" name="postal_code_three" value="{{ old("postal_code_three") }}" required>@error("postal_code_three")<span class="help-block">{{ $message }}</span>@enderror<span id="postal_code_three_error_message" class="help-block"></span><button type="button" onclick="getAddressThree()" class="btn-primary">Get address</button><span class="get-address-error-three"></span></div><div class="form-group @error("address") has-error @enderror"><label for="get_address_three">Address</label><input id="get_address_three" type="text" class="form-control" value="{{ old("address_three") }}" required disabled><input id="address_three" type="hidden" name="address_three" class="form-control" value="{{ old("address_three") }}" required><input id="longitude_three" type="hidden" name="longitude_three" class="form-control" value="{{ old("longitude_three") }}" required><input id="latitude_three" type="hidden" name="latitude_three" class="form-control" value="{{ old("latitude_three") }}" required>@error("address_three")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("unit_no_three") has-error @enderror"><label for="unit_no_three">Unit No (Key ‘0’ if no unit no)</label><input id="unit_no_three" type="text" class="form-control" name="unit_no_three" value="{{ old("unit_no_three") }}" required>@error("unit_no_three")<span class="help-block">{{ $message }}</span>@enderror</div><div class="form-group @error("phone_three") has-error @enderror"><label for="phone_three">Contact</label><input id="phone_three" type="tel" class="form-control" name="phone_three" value="{{ old("phone_three") }}" required>@error("phone_three")<span class="help-block">{{ $message }}</span>@enderror</div></div>');
    		$("div#second-address-field").after(domElement);
    		$("button[name='add-third-delivery-address']").remove();
        }
	                    
		

		function getAddressOne() {
			const postal_code_one = $('#postal_code_one').val();
			if (!postal_code_one)  {
				$('#postal_code_one_error_message').text('Postal Code not found.');
                $('#get_address_one').val('');
                $('#address_one').val('');
			}
			else {
				const pickupAddress = document.getElementById('pickup_address').value;
				$.ajax({
	                type: 'GET',
	                url: "{{route('address.get')}}",
	                data: { postal_code: postal_code_one, pickup: pickupAddress, _token: '{{csrf_token()}}' },
	                success: function(data) {
	                    if (data == '') {
	                    	$('#postal_code_one_error_message').text('Postal Code not found.');
	                    	$('#get_address_one').val('');
	                    	$('#address_one').val('');
	                    	$('#longitude_one').val('');
                    		$('#latitude_one').val('');
	                    	addressOne = 0;
	                    	calculateFee();
	                    }
	                    else {
	                    	$('#postal_code_one_error_message').text('');
	                    	$('#get_address_one').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#address_one').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#longitude_one').val(data.LONGITUDE);
                    		$('#latitude_one').val(data.LATITUDE);
	                    	if (data.fee !== null) {
	                    		addressOne = data.fee;
	                    	}
	                    	else {
	                    		addressOne = 0;
	                    	}
	                    	calculateFee();
	                    	triggered_one = true;
	                    	if(triggered_one == true && triggered_two == true && triggered_three == true && postal_code_one != ''  && postal_code_one.length == 6) {
	                    		$('#submit-delivery').prop('disabled', false);
	                    	}
	                    }
	                    $('span.get-address-error-one').text('');
	                }
	            });	
			}
		}

		function getAddressTwo() {
			const postal_code_two = $('#postal_code_two').val();
			if (!postal_code_two)  {
				$('#postal_code_two_error_message').text('Postal Code not found.');
                $('#get_address_two').val('');
                $('#address_two').val('');
			}
			else {
				const pickupAddress = document.getElementById('pickup_address').value;
				$.ajax({
	                type: 'GET',
	                url: "{{route('address.get')}}",
	                data: { postal_code: postal_code_two, pickup: pickupAddress, _token: '{{csrf_token()}}' },
	                success: function(data) {
	                    if (data == '') {
	                    	$('#postal_code_two_error_message').text('Postal Code not found.');
	                    	$('#get_address_two').val('');
	                    	$('#address_two').val('');
	                    	$('#longitude_two').val('');
                    		$('#latitude_two').val('');
	                    	addressTwo = 0;
	                    	calculateFee();
	                    }
	                    else {
	                    	$('#postal_code_two_error_message').text('');
	                    	$('#get_address_two').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#address_two').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#longitude_two').val(data.LONGITUDE);
                    		$('#latitude_two').val(data.LATITUDE);
	                    	if (data.fee !== null) {
	                    		addressTwo = data.fee;
	                    	}
	                    	else {
	                    		addressTwo = 0;
	                    	}
	                    	calculateFee();
	                    	triggered_two = true;
	                    	if(triggered_one == true && triggered_two == true && triggered_three == true && postal_code_two != ''  && postal_code_two.length == 6) {
	                    		$('#submit-delivery').prop('disabled', false);
	                    	}
	                    }
	                    $('span.get-address-error-two').text('');
	                }
	            });
	        }
		}

		function getAddressThree() {
			const postal_code_three = $('#postal_code_three').val();
			if (!postal_code_three)  {
				$('#postal_code_three_error_message').text('Postal Code not found.');
                $('#get_address_three').val('');
                $('#address_three').val('');
			}
			else {
				const pickupAddress = document.getElementById('pickup_address').value;
				$.ajax({
	                type: 'GET',
	                url: "{{route('address.get')}}",
	                data: { postal_code: postal_code_three, pickup: pickupAddress, _token: '{{csrf_token()}}' },
	                success: function(data) {
	                    if (data == '') {
	                    	$('#postal_code_three_error_message').text('Postal Code not found.');
	                    	$('#get_address_three').val('');
	                    	$('#address_three').val('');
	                    	$('#longitude_three').val('');
                    		$('#latitude_three').val('');
	                    	addressThree = 0;
	                    	calculateFee();
	                    }
	                    else {
	                    	$('#postal_code_three_error_message').text('');
	                    	$('#get_address_three').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#address_three').val(data.BLK_NO+' '+data.ROAD_NAME);
	                    	$('#longitude_three').val(data.LONGITUDE);
                    		$('#latitude_three').val(data.LATITUDE);
	                    	if (data.fee !== null) {
	                    		addressThree = data.fee;
	                    	}
	                    	else {
	                    		addressThree = 0;
	                    	}
	                    	calculateFee();
	                    	triggered_three = true;
	                    	if(triggered_one == true && triggered_two == true && triggered_three == true && postal_code_three != '' && postal_code_three.length == 6) {
	                    		$('#submit-delivery').prop('disabled', false);
	                    	}
	                    }
	                    $('span.get-address-error-three').text('');
	                }
	            });
	        }
		}

		function calculateFee() {
			feeOne = parseFloat(addressOne);
			feeTwo = parseFloat(addressTwo);
			feeThree = parseFloat(addressThree);
			const fee =  feeOne + feeTwo + feeThree;
			$('#get_fee').val(fee);
			$('#fee').val(fee);
		}

		function removeAddressTwo() {
			const domElement = $('<div class="form-group"><button type="button" onclick="addAddressTwo()" name="add-second-delivery-address" class="btn-primary">Add another delivery address</button></div>');
			$("#phone_one_field").after(domElement);
			$("div#second-address-field").remove();
			addressTwo = 0;
			calculateFee();

		}

		function removeAddressThree() {
			$('#remove-address-two-field').prop('disabled', false);
	    	$('#remove-address-two-field').show();
			const domElement = $('<div class="form-group"><button type="button" onclick="addAddressThree()" name="add-third-delivery-address" class="btn-primary">Add another delivery address</button></div>');
			$("#phone_two_field").after(domElement);
			$("div#third-address-field").remove();
			addressThree = 0;
			calculateFee();
		}

	</script>
@endpush