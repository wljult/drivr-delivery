@extends('adminlte::page')

@section('title', 'Address Form')

@section('content_header')
    <h1>Add new address</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('address.store') }}" enctype="multipart/form-data">
	                        @method('POST') 
	                        @csrf
	                        <div class="form-group @error('name') has-error @enderror">
	                            <label for="name">Name</label>
	                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
	                            @error('name')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('postal_code') has-error @enderror">
	                            <label for="postal_code">Postal Code</label>
	                            <input id="postal_code" type="number" class="form-control" name="postal_code" value="{{ old('postal_code') }}" required>
	                            @error('postal_code')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                            <span id="postal_code_error_message" class="help-block"></span>
	                            <button type="button" onclick="getAddress()" class="btn-primary">Get address</button>
	                        </div>

	                        <div class="form-group @error('address') has-error @enderror">
	                            <label for="get_address">Address</label>
	                            <input id="get_address" type="text" class="form-control" value="{{ old('get_address') }}" required disabled>
	                            @error('address')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <input id="address" type="hidden" name="address" class="form-control" value="{{ old('address') }}" required>
	                        <input id="longitude" type="hidden" name="longitude" class="form-control" value="{{ old('longitude') }}" required>
	                        <input id="latitude" type="hidden" name="latitude" class="form-control" value="{{ old('latitude') }}" required>

	                        <div class="form-group @error('unit_no') has-error @enderror">
	                            <label for="unit_no">Unit No (Key ‘0’ if no unit no)</label>
	                            <input id="unit_no" type="text" class="form-control" name="unit_no" value="{{ old('unit_no') }}" required>
	                            @error('unit_no')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('phone') has-error @enderror">
	                            <label for="phone">Contact</label>
	                            <input id="phone" type="tel" class="form-control" name="phone" value="{{ old('phone') }}" required>
	                            @error('phone')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <input type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-address" onclick="disableButton()" type="submit" class="btn btn-primary">Save</button>
	                            <a href="{{ route('address.index')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push('additional_js')
    <script>

    	function disableButton() {
	        var is_empty = false;
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	            $(".lds-dual-ring").show();
	        }
	    }

		function getAddress() {
			var postal_code = $('#postal_code').val();
			
			$.ajax({
                type: 'GET',
                url: "{{route('address.get')}}",
                data: { postal_code: postal_code, _token: '{{csrf_token()}}' },
                success: function(data) {
                	
                    if (data == '') {
                    	$('#postal_code_error_message').text('Postal code not found.');
                    	$('#get_address').val('');
                    	$('#address').val('');
                    	$('#longitude').val('');
                    	$('#latitude').val('');
                    }
                    else {
                    	$('#postal_code_error_message').text('');
                    	$('#get_address').val(data.BLK_NO+' '+data.ROAD_NAME);
                    	$('#address').val(data.BLK_NO+' '+data.ROAD_NAME);
                    	$('#longitude').val(data.LONGITUDE);
                    	$('#latitude').val(data.LATITUDE);
                    }
                }
            });
		}
	</script>
@endpush