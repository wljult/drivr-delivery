@extends('adminlte::page')

@section('title', 'Address List')

@section('content_header')
    <h1>Address list</h1>
@endsection

@section('content')
    <div class="animated slideInUpTiny animation-duration-3">
        <div class="page-heading">
            <h2 class="title">
                @if (!auth()->user()->is_admin)
                <a href="{{ route('address.create') }}">
                    <button type="button" class="btn btn-outline-primary drivr-add-button"><i class="drivr drivr-plus-square"></i> Add new </button>
                </a>
                @endif
            </h2>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="gx-card">
                    <div class="gx-card-header">
                        <h3 class="card-heading"></h3>
                    </div>
                    <div class="gx-card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="table-responsive">                    
                            <table class="table table-striped" id="datatables-address">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Postal code</th>
                                        <th>Address</th>
                                        <th>Unit No</th>
                                        <th>Contact</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('vendor.adminlte.includes.modal-delete')
@endsection

@push('additional_js')
    <!--Datatables JQuery-->
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script>
        function disableButton() {
            var is_empty = false;
            $('input').each(function(){
                if($(this).val() == "" && $(this).prop('required')){
                    is_empty = true;
                }
            })
            if (is_empty == false) {
               $(".lds-dual-ring").show();
            }
        }

        function delete_address() {
            disableButton();
            const id = $('#modalDeleteID').val();
            const currentUrl = window.location.href;
            $.ajax({
                type: 'DELETE',
                url: "{{ route('address.destroy', ['address' => NULL]) }}/" + id,
                data: { address: id, _token: '{{csrf_token()}}' },
                success: function(data) {
                    window.location.href = currentUrl;
                }
            });
        }

        $(document).ready(function () {
            const table = $('#datatables-address').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('address.datatable') }}",
                    data: function (d) {
                        d._token = '{{csrf_token()}}';
                    },
                    type: "POST",
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'postal_code', name: 'postal_code'},
                    {data: 'address', name: 'address'},
                    {data: 'unit_no', name: 'unit_no'},
                    {data: 'phone', name: 'phone'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                "language": {
                  "emptyTable": "There is no address yet."
                },
            });
        });
    </script>
@endpush