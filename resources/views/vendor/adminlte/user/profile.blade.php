@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Profile</h1>
@endsection

@section('content')
	<div class="animated slideInUpTiny animation-duration-3">
	    <div class="row">
	        <div class="col-lg-12">
	            <div class="gx-card">
	                <div class="gx-card-header">
	                    <h3 class="card-heading"></h3>
	                </div>
	                <div class="gx-card-body">
	                	@if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                            </div>
                        @endif
	                    <form method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data">
	                        @method('PUT') 
	                        @csrf
	                        <div class="form-group @error('name') has-error @enderror">
	                            <label for="name">Name</label>
	                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}" required autocomplete="name">
	                            @error('name')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('username') has-error @enderror">
	                            <label for="username">Username</label>
	                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username', $user->username) }}" required autocomplete="username">
	                            @error('username')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="form-group @error('email') has-error @enderror">
	                            <label for="email">Email</label>
	                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" required autocomplete="email" style="text-transform: lowercase;">
	                            @error('email')
	                                <span class="help-block">{{ $message }}</span>
	                            @enderror
	                        </div>

	                        <div class="checkbox">
	                        	<label>
	                        		<input type="hidden" name="change_password" value="0" />
	                                <input type="checkbox" id="change_password" name="change_password" value="1" @if(old('change_password') == 1) checked @endif>
	                        		<strong>Change password</strong>
	                        	</label>
	                        </div>

	                        <div class="form-group form-password @error('password') has-error @enderror" style="display: none;">
                                <label for="password">New Password</label>
                                <input id="password" type="password" class="form-control" name="password">
                                @error('password')
                                    <span class="help-block">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group form-password" style="display: none;">
                                <label for="password-confirm">Confirm New Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                            </div>
	                        
	                        <input type="hidden" name="unique_token" value="{{$unique_token}}">
	                        <div class="form-group">
	                            <button id="submit-profile" onclick="disableButton()" type="submit" class="btn btn-primary">Save</button>
	                            <a href="{{ route('home')}}" class="btn btn-primary">Back</a>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@push('additional_js')
    <script>
    	function disableButton() {
	        var is_empty = false;
	        $('input').each(function(){
	            if($(this).val() == "" && $(this).prop('required')){
	                is_empty = true;
	            }
	        })
	        if (is_empty == false) {
	           $(".lds-dual-ring").show();
	        }
	    }

		$(document).ready(function () {

			$('#change_password').change(function () {
				const isChecked = $('#change_password').prop('checked');
				if (isChecked) {
			        $('.form-password').fadeIn('slow');
			    }
			    else {
			        $('.form-password').fadeOut('slow');
			    }
			    $('#old_password').attr('required', isChecked).val('');
		        $('#password').attr('required', isChecked).val('');
		        $('#password-confirm').attr('required', isChecked).val('');
			});

		    $('#change_password').trigger('change');
		});
	</script>
@endpush