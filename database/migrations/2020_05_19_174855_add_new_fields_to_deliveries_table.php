<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('order_no')->default('');
            $table->string('contact')->default('');
            $table->string('name')->default('');
            $table->string('unit_no')->default('');
            $table->string('postal_code')->default('');
            $table->boolean('post_fail')->default(0);
            $table->boolean('detail_fail')->default(0);
            $table->string('pickup_id')->nullable();
            $table->string('delivery_id')->nullable();
            $table->string('tracking_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('order_no');
            $table->dropColumn('contact');
            $table->dropColumn('name');
            $table->dropColumn('unit_no');
            $table->dropColumn('postal_code');
            $table->dropColumn('post_fail');
            $table->dropColumn('detail_fail');
            $table->dropColumn('pickup_id');
            $table->dropColumn('delivery_id');
            $table->dropColumn('tracking_link');
        });
    }
}
