<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('poster_id');
            $table->foreign('poster_id')->references('id')->on('users');
            $table->unsignedBigInteger('pickup_address_id');
            $table->foreign('pickup_address_id')->references('id')->on('addresses');
            $table->text('pickup_address');
            $table->text('delivery_address');
            $table->date('delivery_date');
            $table->time('delivery_time');
            $table->integer('fee');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
