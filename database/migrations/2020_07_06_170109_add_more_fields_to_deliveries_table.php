<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsToDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('option_one')->default(0);
            $table->string('option_two')->default(0);
            $table->string('option_three')->default(0);
            $table->string('option_four')->default(0);
            $table->string('total_fee')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn('option_one');
            $table->dropColumn('option_two');
            $table->dropColumn('option_three');
            $table->dropColumn('option_four');
            $table->dropColumn('total_fee');
        });
    }
}
