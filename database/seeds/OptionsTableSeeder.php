<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Option::create([
        	'id' => 1,
        	'name' => 'ERP',
        ]);

        Option::create([
        	'id' => 2,
        	'name' => 'Restricted Area',
        ]);

        Option::create([
        	'id' => 3,
        	'name' => 'Waiting/Late Fee',
        ]);

        Option::create([
        	'id' => 4,
        	'name' => 'Others',
        ]);
    }
}
