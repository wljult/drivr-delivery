<?php

use Illuminate\Database\Seeder;
use App\Models\Delivery;

class UpdateTotalFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deliveries = Delivery::all();
        foreach ($deliveries as $delivery) {
        	$delivery->total_fee = $delivery->fee;
        	$delivery->save();
        }
    }
}
