<?php

use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::create([
        	'name' => 'token',
        	'value' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ0NzksInVzZXJfaWQiOjQ0NzksImVtYWlsIjoid2wuanVsdEBnbWFpbC5jb20iLCJmb3JldmVyIjpmYWxzZSwiaXNzIjoiaHR0cDpcL1wvb20yLmRmZS5vbmVtYXAuc2dcL2FwaVwvdjJcL3VzZXJcL3Nlc3Npb24iLCJpYXQiOjE1ODk3NjAwMDIsImV4cCI6MTU5MDE5MjAwMiwibmJmIjoxNTg5NzYwMDAyLCJqdGkiOiJjNTY3YmViODJkNzg0ZThmZGRlYzFjMzVkY2Q0ZmFlNiJ9.nE4n9FlcZjCSh3iA7l-kYWH4FtjuR0EES_wjPFMqwtg',
        ]);

        Config::create([
        	'name' => 'email',
        	'value' => 'wl.jult@gmail.com',
        ]);

        Config::create([
        	'name' => 'password',
        	'value' => 'xA24gsdgTkMUnnQ',
        ]);

        Config::create([
        	'name' => 'admin_email',
        	'value' => 'wl.jult@gmail.com',
        ]);

        Config::create([
        	'name' => 'api_key',
        	'value' => '5661618cf14642165517767e5710254314edc3f223da7e375e1a06',
        ]);

        Config::create([
        	'name' => 'api_secret',
        	'value' => '5661618cf14642165517767e5710254314edc3f223da7e375e1a06',
        ]);
    }
}
