<?php

use Illuminate\Database\Seeder;
use App\Models\Fee;

class FeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fee::create([
        	'value_in_sgd' => 10,
        	'min_distance' => 0,
        	'max_distance' => 7999,
        ]);

        Fee::create([
        	'value_in_sgd' => 14,
        	'min_distance' => 8000,
        	'max_distance' => 11999,
        ]);

        Fee::create([
        	'value_in_sgd' => 17,
        	'min_distance' => 12000,
        	'max_distance' => 14999,
        ]);

        Fee::create([
            'value_in_sgd' => 22,
            'min_distance' => 15000,
            'max_distance' => 19999,
        ]);

        Fee::create([
            'value_in_sgd' => 24,
            'min_distance' => 20000,
            'max_distance' => 24999,
        ]);

        Fee::create([
            'value_in_sgd' => 24,
            'min_distance' => 25000,
            'max_distance' => 999999,
        ]);
    }
}
